<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateChoosesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chooses', function (Blueprint $table) {

            $table->string('header_ar');
            $table->string('header_fr');
            $table->text('content_ar');
            $table->text('content_fr');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chooses', function (Blueprint $table) {
            $table->dropColumn('header_ar');
            $table->dropColumn('header_fr');
            $table->dropColumn('content_ar');
            $table->dropColumn('content_fr');

        });
    }
}
