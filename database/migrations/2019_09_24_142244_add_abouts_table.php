<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAboutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('abouts', function (Blueprint $table) {

            $table->string('title_ar');
            $table->string('title_fr');
            $table->text('content_ar');
            $table->text('content_fr');
            $table->text('description_ar');
            $table->text('description_fr');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('abouts', function (Blueprint $table) {
            $table->dropColumn('title_ar');
            $table->dropColumn('title_fr');
            $table->dropColumn('content_ar');
            $table->dropColumn('content_fr');
            $table->dropColumn('description_ar');
            $table->dropColumn('description_fr');
        });
    }
}
