<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateManufacturingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('manufacturing', function (Blueprint $table) {
            $table->string('header_ar')->nullable();
            $table->text('content_ar')->nullable();
            $table->string('header_fr')->nullable();
            $table->text('content_fr')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('manufacturing', function (Blueprint $table) {
            $table->dropColumn('header_ar')->nullable();
            $table->dropColumn('content_ar')->nullable();
            $table->dropColumn('header_fr')->nullable();
            $table->dropColumn('content_fr')->nullable();

        });
    }
}
