<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCompanyCapitalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_capitals', function (Blueprint $table) {
            $table->string('en_name')->nullable();
            $table->string('fr_name')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_capitals', function (Blueprint $table) {
            $table->dropColumn('en_name')->nullable();
            $table->dropColumn('fr_name')->nullable();
        });

    }
}
