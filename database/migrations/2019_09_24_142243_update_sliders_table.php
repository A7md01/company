<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sliders', function (Blueprint $table) {

            $table->string('title_ar');
            $table->string('title_fr');
            $table->text('description_ar');
            $table->text('description_fr');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('sliders', function (Blueprint $table) {
            $table->dropColumn('title_ar');
            $table->dropColumn('title_fr');
            $table->dropColumn('description_ar');
            $table->dropColumn('description_fr');
        });
    }
}
