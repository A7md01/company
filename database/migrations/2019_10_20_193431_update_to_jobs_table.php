<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateToJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jobs', function (Blueprint $table) {
            $table->string('job_title_en')->nullable();
            $table->string('years_of_experience_en')->nullable();
            $table->text('job_description_en')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jobs', function (Blueprint $table) {
            $table->dropColumn('job_title_en')->nullable();
            $table->dropColumn('years_of_experience_en')->nullable();
            $table->dropColumn('job_description_en')->nullable();
        });

    }
}
