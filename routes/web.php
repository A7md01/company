<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::group(['namespace' => 'Front' ,
             'prefix' =>'{locale}' ,
             'where' => ['locale' =>'[a-zA-Z]{2}'],

        ], function () {
    Route::get('/', 'HomeController@index');
    Route::get('/about', 'HomeController@about');
    Route::get('/manufacturing', 'HomeController@manufacturing');
    Route::get('/contactUs', 'HomeController@contactUs');
    Route::get('/product', 'HomeController@product');
    Route::get('/service', 'HomeController@service');
    Route::get('/jobs', 'HomeController@job');
    Route::get('/register_form', 'HomeController@register');

    Auth::routes();

    Route::get('locale/{locale}', function ($locale){
        Session::put('locale', $locale);
        return redirect()->back();
    });

});
*/
Route::group(['namespace' => 'Front'], function () {
    Route::get('/', 'HomeController@index');
    Route::get('/about', 'HomeController@about');
    Route::get('/manufacturing', 'HomeController@manufacturing');
    Route::get('/contactUs', 'HomeController@contactUs');
    Route::get('/product', 'HomeController@product');
    Route::get('/service', 'HomeController@service');
    Route::get('/jobs', 'HomeController@job');
    Route::get('/charts', 'HomeController@charts');
    Route::get('/capitals', 'HomeController@capital');

    Route::get('/register_form', 'HomeController@register');


    // Route::get('contact-us', 'ContactController@contactUs');
    // Route::post('contact-us', 'ContactController@contactUsPost')->name('contactus.store');

    Route::get('locale/{locale}', function ($locale){
        Session::put('locale', $locale);
        return redirect()->back();
    });

});
Auth::routes();
Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('/', 'DashboardController@index');
    Route::resource('/sliders', 'SliderController');
    Route::resource('/services', 'ServiceController');
    Route::resource('/portfolios', 'PortfolioController');
    Route::resource('/teams', 'TeamController');
    Route::resource('/testimonials', 'TestimonialController');
    Route::resource('/reports', 'ReportController');
    Route::resource('/abouts', 'AboutController');
    Route::resource('/statistics', 'StatisticController');
    Route::resource('/advisors', 'AdvisorController');
    Route::resource('/chooses', 'ChooseController');
    Route::resource('/products', 'ProductController');
    Route::resource('/manufacturing', 'ManufacturingController');
    Route::resource('/jobs', 'JobController');
    Route::resource('/charts', 'OrganizationChartController');
    Route::resource('/capitals', 'CapitalController');


});
Route::get('/home', 'HomeController@index')->name('home');

