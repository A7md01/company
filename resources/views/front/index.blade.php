@extends('front.layouts.app')
@section('content')

	<!--Main Slider-->
    <section class="main-slider">

        <div class="main-slider-carousel owl-carousel owl-theme">
            @foreach ($sliders as $slider)
                <div class="slide" style="background-image:url({{ asset('/'.$slider->image) }} )">
                    <div class="auto-container">
                        <div class="content clearfix">
                            @if (App::isLocale('en'))
                                <h1>{{ $slider->title }}</h1>
                                <div class="text"> {{ $slider->description }} </div>
                            @elseif (App::isLocale('ar'))
                                <h1>{{ $slider->title_ar }}</h1>
                                <div class="text"> {{ $slider->description_ar }} </div>
                            @else
                                <h1>{{ $slider->title_fr }}</h1>
                                <div class="text"> {{ $slider->description_fr }} </div>
                            @endif
                            <div class="link-box">
                                <a href="{{ url('/about') }}" class="theme-btn btn-style-one">
                                    <span class="txt">{{ __('message.about_com') }}</span>
                                </a>
                                <a href="{{ url('/contactUs') }}" class="theme-btn btn-style-two">
                                    <span class="txt">{{ __('message.contactUs')}} </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
    <!--End Main Slider-->

	<!--About Section-->
    <section class="about-section-two">
    	<div class="auto-container">
        	<div class="row clearfix">
            	<!--Column-->
            	<div class="column col-lg-7 col-md-12 col-sm-12">
                	<div class="about-content-box">
                        @foreach ($abouts as $about)
                    	<div class="sec-title">
                            <h2>{{ __('message.about')}}</h2>
                        </div>
                        @if (App::isLocale('en'))
                        <h4><span class="number">{{$about->number_expertise }}</span>{{$about->title }}</h4>
                        <div class="text">
                            {{$about->content }}
                        </div>
                        <a style="background-color:black;" href="{{url('/about') }}" class="theme-btn btn-style-two">
                            <span class="txt">{{__('message.see_more') }} </span>
                        </a>
                        @elseif (App::isLocale('ar'))
                        <h3><span class="number" style="float:right"> &nbsp;{{$about->number_expertise }}</span> {{$about->title_ar }}</h3>
                        <div class="text">
                            {{$about->content_ar }}
                            <a style="background-color:black;float:left" href="{{url('/about') }}" class="theme-btn btn-style-two">
                                <span class="txt">{{__('message.see_more') }} </span>
                            </a>
                        </div>
                        @else
                        <h4><span class="number">{{$about->number_expertise }}</span>{{$about->title_fr }}</h4>
                        <div class="text">
                            {{$about->content_fr }}
                        </div>
                        <a style="background-color:black;" href="{{url('/about') }}" class="theme-btn btn-style-two">
                            <span class="txt">{{__('message.see_more') }} </span>
                        </a>
                        @endif
                    </div>

                </div>
                <!--Column-->
                <div class="column col-lg-5 col-md-12 col-sm-12">
                    <div class="image-box">
                        <figure class="image">
                            <img src="{{ asset('/'.$about->image) }}" alt="" />
                        </figure>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </section>

    <!--Fluid Section One-->
    <section class="fluid-section-one">
    	<div class="outer-container clearfix">

            <!--Image Column-->
            <div class="image-column" style="background-image:url( {{asset('assets/front')}}/images/resource/image-1.jpg);">
            	<figure class="image-box"><img src="{{ asset('assets/front') }}/images/resource/image-1.jpg" alt=""></figure>
            </div>



        	<!--Content Column-->
            <div class="content-column">
            	<div class="inner-column">
                    <h2>{{ __('message.Why Choose ASCE')}}</h2>
                    <h3 style=""rerer</h3>
                    @foreach ($chooses as $choose)
					<div class="text">
                        @if (App::isLocale('en'))
                            <strong>{{$choose->header }} : </strong> {{$choose->content }}
                        @elseif (App::isLocale('ar'))
                            <strong>{{$choose->header_ar }} : </strong> {{$choose->content_ar }}
                        @else
                            <strong>{{$choose->header_fr }} : </strong> {{$choose->content_fr }}
                        @endif
                    </div>
                    @endforeach
				</div>
			</div>
		</div>
    </section>


    <!-- products -->
    <section class="news-section">
            <div class="auto-container">
                <!--Sec Title-->
                <div class="sec-title centered">
                    <h2>{{__('message.products')}}</h2>
                </div>
                <div class="row clearfix">
                    <!--News Block-->
                    @foreach ($products as $product)
                    <div class="news-block col-lg-4 col-md-6 col-sm-12">
                        <div class="inner-box">
                            <div class="image">
                                <img src="{{ asset('/'.$product->image) }}" alt="" />
                            </div>
                            @if (App::isLocale('en'))
                            <div class="lower-content">
                                <h5 style="text-align:center"><a>{{ $product->title }}</a></h5>
                            </div>
                            @elseif (App::isLocale('ar'))
                            <div class="lower-content">
                                <h5 style="text-align:center"><a>{{ $product->title_ar }}</a></h5>
                            </div>
                            @else
                            <div class="lower-content">
                                <h5 style="text-align:center"><a>{{ $product->title_fr }}</a></h5>
                            </div>
                            @endif

                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
    </section>

	<!--Fun Facts Section-->
    <div class="fact-counter-section">
        <div class="fact-counter">
            <div class="auto-container">
                <div class="row clearfix">
                    @foreach ($statistics as $statistic)
                        <div class="column counter-column col-lg-3 col-md-6 col-sm-12">
                            <div class="inner">
                                <div class="count-outer count-box">
                                    <span class="count-text" data-speed="3000" data-stop="{{$statistic->statistics_no}}">0</span>
                                    @if (App::isLocale('en'))
                                        <h5 class="counter-title"> {{$statistic->title }}</h5>
                                    @elseif (App::isLocale('ar'))
                                        <h5 class="counter-title"> {{$statistic->title_ar }}</h5>
                                    @else
                                        <h5 class="counter-title"> {{$statistic->title_fr }}</h5>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!--End Fun Facts Section-->
	<!--Team Section-->
	{{-- <section class="team-section">
		<div class="auto-container">
			<!--Sec Title-->
			<div class="sec-title centered">
				<h2>{{ __('message.advisors')}}</h2>
			</div>

			<div class="row clearfix">
                @foreach ($advisors as $advisor)
                    <!--Team Member-->
                    <div class="team-member col-lg-3 col-md-6 col-sm-12">
                        <div class="inner-box wow fadeIn" data-wow-delay="0ms" data-wow-duration="1500ms">
                            <div class="image-box">
                                <figure class="image"><img src="{{ asset('/'.$advisor->image) }}" alt=""></figure>
                            </div>
                            <div class="lower-content">
                                @if (App::isLocale('en'))
                                    <h6><a>{{$advisor->name }}</a></h6>
                                    <div class="designation">{{$advisor->job_title}}</div>
                                @elseif (App::isLocale('ar'))
                                    <h6><a>{{$advisor->name_ar }}</a></h6>
                                    <div class="designation">{{$advisor->job_title_ar}}</div>
                                @else
                                    <h6><a>{{$advisor->name }}</a></h6>
                                    <div class="designation">{{$advisor->job_title_fr}}</div>
                                @endif
                                <h6>{{$advisor->email }}</h6>
                                <h6>{{$advisor->phone_number }}</h6>
                            </div>
                        </div>
                    </div>
                @endforeach
			</div>

		</div>
    </section> --}}
    <!--End Team Section-->

@endsection
