<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }} ">
<head>
<meta charset="utf-8">
<meta name="csrf-token" content="{{ csrf_token() }}">

<title> ASCE | Homepage </title>
<!-- Stylesheets -->
@if (App::isLocale('ar'))
<link href="{{ asset('assets/front/css/bootstraprtl.css') }}" rel="stylesheet">
<link href="{{ asset('assets/front/css/stylertl.css') }}" rel="stylesheet">

@else
<link href="{{ asset('assets/front/css/bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('assets/front/css/style.css') }}" rel="stylesheet">
@endif
<link href="{{ asset('assets/front/css/slick.css') }}" rel="stylesheet">
<link href="{{ asset('assets/front/css/responsive.css') }}" rel="stylesheet">
<!--Color Switcher Mockup-->
<link href="{{ asset('assets/front/css/color-switcher-design.css') }}" rel="stylesheet">
<!--Color Themes-->
<link id="theme-color-file" href="{{ asset('assets/front/css/color-themes/default-theme.css') }}" rel="stylesheet">

<link rel="shortcut icon" href="{{asset('assets/front') }}/images/favicon.png" type="image/x-icon">
<link rel="icon" href="{{ asset('assets/front') }}/images/favicon.png" type="image/x-icon">

<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body class="hidden-bar-wrapper">

<div class="page-wrapper">

    <!-- Preloader -->
    <div class="preloader"></div>

	<!-- Main Header-->
    <header class="main-header header-style-two">

    	<!--Header Top-->
    	<div class="header-top">
    		<div class="auto-container">
				<div class="clearfix">
					<!--Top Left-->
					<div class="top-left">
                        <div class="text">{{ __('message.welcome')}}</div>
                    </div>

					<!--Top Right-->
					<div class="top-right">
                        <div class="language dropdown">
                            @if(App::islocale('en'))
                            <a class="btn btn-default dropdown-toggle" id="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" href="#">
                                <span class="flag-icon">
                                    <img src=" {{ asset('assets/front') }}/images/icons/en.png" alt=""/>
                                </span>English &nbsp;
                                <span class="icon fa fa-angle-down"></span>
                            </a>
                            @elseif(App::islocale('ar'))
                            <a class="btn btn-default dropdown-toggle" id="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" href="#">
                                <span class="flag-icon">
                                    <img src=" {{ asset('assets/front') }}/images/icons/ar.png" alt=""/>
                                </span>العربية &nbsp;
                                <span class="icon fa fa-angle-down"></span>
                            </a>
                            @else
                            <a class="btn btn-default dropdown-toggle" id="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" href="#">
                                <span class="flag-icon">
                                    <img src=" {{ asset('assets/front') }}/images/icons/fr.png" alt=""/>
                                </span>French &nbsp;
                                <span class="icon fa fa-angle-down"></span>
                            </a>
                            @endif

                            <ul class="dropdown-menu style-one" id="languageSwitcher">
                                <li><a href="{{ url('locale/en') }}">{{ __('message.en') }}</a></li>
                                <li><a href="{{ url('locale/ar') }}">{{ __('message.ar') }}</a></li>
                                <li><a href="{{ url('locale/fr') }}">{{ __('message.fr') }}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!--Header-Upper-->
    <div class="header-upper">
        <div class="auto-container">
            <div class="clearfix">

                <div class="pull-left logo-box">
                    <div class="logo">
                        <a href="{{url('/')}}">
                            <img style="width:80px;height:100px;" src="{{ asset('assets/front') }}/images/logo.png" alt="" >
                        </a>
                        <span> {{ __('message.company_name' ) }} </span>
                    </div>
                </div>

                <div class="pull-right upper-right clearfix">

                    <!--Info Box-->
                    <div class="upper-column info-box">
                        <div class="icon-box"><span class="flaticon-pin"></span></div>
                        <ul>
                            <li><a href="{{ url('/contactUs') }}"> 15 Al Ahram St. Roxi <br>Heliopolis , Cairo , Egypt</a></li>
                        </ul>
                    </div>

                    <!--Info Box-->
                    <div class="upper-column info-box">
                        <div class="icon-box"><span class="flaticon-technology-1"></span></div>
                        <ul>
                            <li style="text-transform:lowercase;" >02 22906 425 - 02 22909 716 <br> info@asce-eg.com</li>
                        </ul>
                    </div>

                    <!--Info Box-->
                    <div class="upper-column info-box">
                        <div class="icon-box"><span class="flaticon-clock"></span></div>
                        <ul>
                            <li> 09:00am to 5:00pm <br> Friday & Satarday Closed</li>
                        </ul>
                    </div>

                </div>

            </div>
        </div>
    </div>
        <!--End Header Upper-->
        <!--Header Lower-->
        <div class="header-lower">

        	<div class="auto-container">
            	<div class="nav-outer clearfix">
                    <!-- Main Menu -->
                    <nav class="main-menu navbar-expand-md">
                        <div class="navbar-header">
                            <!-- Toggle Button -->
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <div class="navbar-collapse collapse clearfix" id="navbarSupportedContent">
                            <ul class="navigation clearfix">
                                <li ><a href="{{ url('/') }}"> {{ __('message.home') }}</a></li>
                                <li class="dropdown"><a href="{{ url('/about') }}"> {{ __('message.about_com') }}</a>
                                    <ul>
                                        <li><a href="{{ url('/charts') }}">{{ __('message.organization_chart') }}</a></li>
										<li><a href="{{ url('/capitals') }}">{{ __('message.capital') }}</a></li>

                                    </ul>
                                </li>
								<li><a href="{{url('/manufacturing')}}">{{ __('message.manufacturing') }}</a></li>
                                <li><a href="{{url('/service')}}"> {{ __('message.services') }}</a></li>
                                <li><a href="{{url('/product') }}">{{ __('message.products') }}</a></li>
								<li><a href="{{ url('/contactUs')}}">{{ __('message.contactUs') }}</a></li>
								<li><a href="{{ url('/jobs')}}">{{ __('message.job') }}</a></li>
                            </ul>
                        </div>

                    </nav>


					<div class="side-curve"></div>
                </div>
            </div>
        </div>
        <!--End Header Lower-->

        <!--Sticky Header-->
        <div class="sticky-header">
        	<div class="auto-container clearfix">
            	<!--Logo-->
            	<div class="logo pull-left">
                	<a href="{{url('/')}}" class="img-responsive"><img style="width:60px;height:70px;" src="{{ asset('assets/front') }}/images/logo-small.png" alt="" title="">{{__('message.company_name') }}</a>
                </div>

                <!--Right Col-->
                <div class="right-col pull-right">
                	<!-- Main Menu -->
                    <nav class="main-menu navbar-expand-md">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <div class="navbar-collapse collapse clearfix" id="navbarSupportedContent1">
                            <ul class="navigation clearfix">
                                <li><a href="{{ url('/') }}"> {{ __('message.home') }}</a></li>
                                <li class="dropdown"><a href="{{ url('/about') }}"> {{ __('message.about_com') }}</a>
                                    <ul>
                                        <li><a href="{{ url('/charts') }}">{{ __('message.organization_chart') }}</a></li>
                                        <li><a href="{{ url('/capitals') }}">{{ __('message.capital') }}</a></li>
                                    </ul>
                                </li>
                                <li><a href="{{url('/manufacturing')}}">{{ __('message.manufacturing') }}</a></li>
                                <li><a href="{{url('/service')}}"> {{ __('message.services') }}</a></li>
                                <li><a href="{{url('/product') }}">{{ __('message.products') }}</a></li>
                                <li><a href="{{ url('/contactUs') }}">  {{ __('message.contactUs') }} </a></li>
								<li><a href="{{ url('/jobs')}}">{{ __('message.job') }}</a></li>

                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>

            </div>
        </div>
        <!--End Sticky Header-->

    </header>
    <!--End Main Header -->


    @yield('content')

	<!--Main Footer-->
    <div class="bottom-parallax">
		<footer class="main-footer">
			<!--Widgets Section-->
			<div class="auto-container">
				<!--Footer Info Section-->
				<div class="footer-info-section">
					<div class="row clearfix">
						<!--Info Block-->
						<div class="info-block col-md-4 col-sm-6 col-xs-12">
							<div class="inner">
								<div class="icon flaticon-pin"></div>
								<h6>{{__('message.address')}}</h6>
                                <div class="text">15 Al Ahram St. Roxi , Heliopolis , Cairo , Egypt</div>
                                <h6>{{__('message.factory') }}</h6>
								<div class="text">Abu Zapal , Qaliobia , Cairo , Egypt</div>
							</div>
						</div>
						<!--Info Block-->
						<div class="info-block col-md-4 col-sm-6 col-xs-12">
							<div class="inner">
								<div class="icon flaticon-technology-1"></div>
								<h6>{{__('message.callUS')}}</h6>
                                <div class="text"> 02 22906 425 - 02 22909 716  </div>
                                <h6>{{__('message.fax')}}</h6>
								<div class="text">02 22919 954</div>

							</div>
						</div>

						<!--Info Block-->
						<div class="info-block col-md-4 col-sm-6 col-xs-12">
							<div class="inner">
								<div class="icon flaticon-web"></div>
								<h6>{{__('message.email')}}</h6>
								<div class="text"> info@asce-eg.com</div>
							</div>
						</div>

					</div>
				</div>

			</div>

			<!--Footer Bottom-->
			<div class="footer-bottom">
				<div class="auto-container">
					<div class="row clearfix">
						<div class="column col-md-6 col-sm-12 col-xs-12">
							<div class="copyright">Copyrights &copy; 2019 All Rights Reserved. Powered by <a href="#">Developer Team in ASCE</a></div>
						</div>
					</div>
				</div>
			</div>
			<!--End Footer Bottom-->

		</footer>
	</div>

</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-arrow-up"></span></div>


<script src="{{ asset('assets/front/js/jquery.js') }}"></script>
<script src="{{ asset('assets/front/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/front/js/bootstrap.min.js') }}"></script>

<script src="{{ asset('assets/front/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script src="{{ asset('assets/front/js/jquery.fancybox.js') }}"></script>
<script src="{{ asset('assets/front/js/appear.js') }}"></script>
<script src="{{ asset('assets/front/js/owl.js') }}"></script>
<script src="{{ asset('assets/front/js/wow.js') }}"></script>
<script src="{{ asset('assets/front/js/slick.js') }}"></script>
<script src="{{ asset('assets/front/js/jquery-ui.js') }}"></script>
<script src="{{ asset('assets/front/js/script.js') }}"></script>
<script src="{{ asset('assets/front/js/custom.js') }}"></script>

<!--Google Map APi Key-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-CE0deH3Jhj6GN4YvdCFZS7DpbXexzGU"></script>
<script src="{{ asset('assets/front/js/map-script.js') }}"></script>
<!--End Google Map APi-->

<script src="js/color-settings.js"></script>

</body>
</html>
