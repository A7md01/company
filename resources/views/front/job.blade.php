@extends('front.layouts.app')
@section('content')
<section class="page-title" style="background-image:url( {{ asset('assets/front/images/background/1.jpg')}}) ">
    <div class="auto-container">
        <h2>{{ __('message.job') }}</h2>
    </div>
</section>
    <div class="breadcrumb-outer">
    	<div class="auto-container">
        	<ul class="bread-crumb text-center">
                <li><a href="{{ url('/')}}">{{__('message.home')}}</a> <span class="fa fa-angle-right"></span></li>
                <li>{{ __('message.job') }} </li>
            </ul>
        </div>
    </div>
<section class="news-section">
    <div class="auto-container">
        <div class="sec-title centered">
            <h2> {{__('message.job_available')}} </h2>
        </div>
        <div class="row clearfix">
            @foreach ($jobs as $job)
            <div class="news-block col-lg-4 col-md-6 col-sm-12"  >
                <div class="inner-box" >
                    @if (App::isLocale('ar'))
                    <div class="lower-content">
                        <div>
                            <h5 style="color:#13B5EA"> {{$job->job_title}} </h5>
                        </div>
                        <div class="text" style="margin-bottom:10px"> <b> {{__('message.age')}} </b> : {{ $job->job_code}}      </div>
                        <div class="text" style="margin-bottom:10px"> <b> {{ __('message.years_of_experience')}} </b> : {{ $job->years_of_experience}}</div>
                        <div class="text" style="margin-bottom:10px"> <b>{{__('message.Conditions_of_joining_the_job') }} </b> : {{$job->job_description }} </div>
                        {{-- <ul class="post-date" >
                            <li style="margin-top:20px"> اخر موعد للتسجيل : {{$job->register_end_date }}</li>
                        </ul> --}}
                        {{-- <a style="background-color:black" href="{{url('/register_form') }}" class="theme-btn btn-style-two">
                            <span class="txt">سجل الان </span>
                        </a> --}}
                    </div>
                    @else
                    <div class="lower-content">
                        <div>
                            <h5 style="color:#13B5EA"> {{$job->job_title_en}} </h5>
                        </div>
                        <div class="text" style="margin-bottom:10px"> <b> {{__('message.age')}} </b> : {{ $job->job_code_en}}      </div>
                        <div class="text" style="margin-bottom:10px"> <b> {{ __('message.years_of_experience')}} </b> : {{ $job->years_of_experience_en}}</div>
                        <div class="text" style="margin-bottom:10px"> <b>{{__('message.Conditions_of_joining_the_job') }} </b> : {{$job->job_description_en }} </div>
                        {{-- <ul class="post-date" >
                            <li style="margin-top:20px"> اخر موعد للتسجيل : {{$job->register_end_date }}</li>
                        </ul> --}}
                        {{-- <a style="background-color:black" href="{{url('/register_form') }}" class="theme-btn btn-style-two">
                            <span class="txt">سجل الان </span>
                        </a> --}}
                    </div>
                    @endif
                </div>
            </div>
            @endforeach
        </div>
        <hr>
        <div class="sec-title centered">
            <h2> {{ __('message.send_cv')}} </h2>
            <h3> <b> hr@asce-eg.com </b> </h>
            <h3> {{__('message.send_subject')}}</h>
        </div>
    </div>
</section>

@endsection
