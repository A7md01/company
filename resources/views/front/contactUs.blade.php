@extends('front.layouts.app')
@section('content')

	<!--Page Title-->
    <section class="page-title" style="background-image:url({{asset('assets/front/images/background/1.jpg') }});">
    	<div class="auto-container">
        	<h2>{{__('message.contactUs') }} </h2>
        </div>
    </section>

    <!--Breadcrumb-->
    <div class="breadcrumb-outer">
    	<div class="auto-container">
        	<ul class="bread-crumb text-center">
            	<li><a href=" {{url('/')}} ">{{__('message.home') }} </a> <span class="fa fa-angle-right"></span></li>
                <li>{{__('message.contactUs') }} </li>
            </ul>
        </div>
    </div>
    <!--End Page Title-->
    	<!--Contact Section-->
        <section class="contact-page-section">
                <div class="auto-container">
                    <div class="sec-title">
                        <h2>{{__('message.contactUs') }} </h2>
                    </div>
                    <div class="gallery-image">
                        <div class="row clearfix">
                            <div class="image-column col-lg-6 col-md-6 col-sm-12">
                                <!--Google Maps-->
                                <div class="mapouter">
                                    <div class="gmap_canvas">
                                        <iframe width="400" height="400" id="gmap_canvas" src="https://maps.google.com/maps?q=Egyptian%20Aluminum%20Sulphate%20Co.&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                                        <a href="https://www.utilitysavingexpert.com"></a>
                                    </div>
                                    <style>.mapouter{position:relative;text-align:right;height:400px;width:400px;}.gmap_canvas {overflow:hidden;background:none!important;height:400px;width:400px;}</style>
                                </div>
                            <!--Google Maps-->
                            </div>
                            <div class="image-column col-lg-6 col-md-6 col-sm-12">
                                <div class="mapouter">
                                    <div class="gmap_canvas">
                                        <iframe width="400" height="400" id="gmap_canvas" src="https://maps.google.com/maps?q=Egyptian%20alum%20factory&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                                        <a href="https://www.easy-ptable.com">periodic table</a>
                                    </div>
                                    <style>.mapouter{position:relative;text-align:right;height:400px;width:400px;}.gmap_canvas {overflow:hidden;background:none!important;height:400px;width:400px;}</style>
                                </div>
                            </div>
                            <!--Image Column-->
                            {{-- <div class="image-column col-lg-3 col-md-3 col-sm-12">
                                <h4> {{__('message.address') }}</h4>
                                <div class="text">15 Al Ahram St. Roxi , Cairo , Egypt</div>
                                <h4>{{__('message.factory') }}</h4>
                                <div class="text">Abu Zapal , Kaliobia , Cairo , Egypt</div>
                                <h4>{{__('message.callUS') }}</h4>
                                <div class="text"> 02 22906 425 - 02 22909 716  </div>
                                <h4>{{__('message.fax') }}</h4>
                                <div class="text">02 22919 954</div>
                                <h4>{{__('message.email') }}</h4>
                                <div class="text"> info@asce-eg.com</div>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
            <!--End Faq Section-->
@endsection
