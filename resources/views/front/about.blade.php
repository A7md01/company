@extends('front.layouts.app')
@section('content')

	<!--Page Title-->
    <section class="page-title" style="background-image:url( {{ asset('assets/front/images/background/1.jpg')}}) ">
    	<div class="auto-container">
        	<h2>{{ __('message.about_com') }}</h2>
        </div>
    </section>

    <!--Breadcrumb-->
    <div class="breadcrumb-outer">
    	<div class="auto-container">
        	<ul class="bread-crumb text-center">
                <li><a href="{{ url('/')}}">{{__('message.home')}}</a> <span class="fa fa-angle-right"></span></li>
                <li>{{ __('message.about_com') }} </li>
            </ul>
        </div>
    </div>
    <!--End Page Title-->

	<!--About Section-->
	<section class="about-section">
		<div class="auto-container">
			<div class="row clearfix">
                @foreach ($abouts as $about)
				<!--Content Column-->
				<div class="content-column col-lg-6 col-md-12 col-sm-12">
					<div class="inner-column">
						<div class="sec-title">
							<h2>{{ __('message.about')}}</h2>
						</div>
						<div class="text">
                            @if (App::isLocale('en'))
                                <p> {{ $about->content}}</p>
                                <p> {{ $about->description}}</p>
                            @elseif (App::isLocale('ar'))
                                <p> {{ $about->content_ar}}</p>
                                <p> {{ $about->description_ar}}</p>
                            @else
                                <p> {{ $about->content_fr}}</p>
                                <p> {{ $about->description_fr}}</p>
                            @endif
						</div>
					</div>
                </div>
				<!--Video Column-->
				<div class="video-column col-lg-6 col-md-12 col-sm-12">
					<div class="inner-column">

						<!--Video Box-->
                        <div class="video-box">
                            <figure class="image">
                                <img src=" {{ asset('/'.$about->image) }} " alt="">
                            </figure>
                        </div>

					</div>
                </div>
                @endforeach

			</div>
		</div>
	</section>
	<!--End About Section-->

    <!--Fluid Section One-->
    <section class="fluid-section-one">
        <div class="outer-container clearfix">
            <!--Image Column-->
            <div class="image-column" style="background-image:url( {{asset('assets/front')}}/images/resource/image-1.jpg);">
                <figure class="image-box"><img src="{{ asset('assets/front') }}/images/resource/image-1.jpg" alt=""></figure>
            </div>
            <!--Content Column-->
            <div class="content-column">
                <div class="inner-column">
                    <h2>{{__('message.Why Choose ASCE') }}</h2>
                    <h3 style=""rerer</h3>
                    @foreach ($chooses as $choose)
                    <div class="text">
                        @if (App::isLocale('en'))
                            <strong>{{$choose->header }} : </strong> {{$choose->content }}
                        @elseif (App::isLocale('ar'))
                            <strong>{{$choose->header_ar }} : </strong> {{$choose->content_ar }}
                        @else
                            <strong>{{$choose->header_fr }} : </strong> {{$choose->content_fr }}
                        @endif
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

	<!--Team Section-->
	<section class="team-section">
            {{-- <div class="auto-container">
                <!--Sec Title-->
                <div class="sec-title centered">
                    <h2>{{ __('message.advisors')}}</h2>
                </div>

                <div class="row clearfix">
                    @foreach ($advisors as $advisor)
                        <!--Team Member-->
                        <div class="team-member col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box wow fadeIn" data-wow-delay="0ms" data-wow-duration="1500ms">
                                <div class="image-box">
                                    <figure class="image"><img src="{{ asset('/'.$advisor->image) }}" alt=""></figure>
                                </div>
                                <div class="lower-content">
                                    @if (App::isLocale('en'))
                                        <h6><a>{{$advisor->name }}</a></h6>
                                        <div class="designation">{{$advisor->job_title}}</div>
                                    @elseif (App::isLocale('ar'))
                                        <h6><a>{{$advisor->name_ar }}</a></h6>
                                        <div class="designation">{{$advisor->job_title_ar}}</div>
                                    @else
                                        <h6><a>{{$advisor->name }}</a></h6>
                                        <div class="designation">{{$advisor->job_title_fr}}</div>
                                    @endif
                                    <h6>{{$advisor->email }}</h6>
                                    <h6>{{$advisor->phone_number }}</h6>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

            </div> --}}
    </section>
        <!--End Team Section-->

@endsection
