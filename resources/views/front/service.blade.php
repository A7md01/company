@extends('front.layouts.app')
@section('content')

	<!--Page Title-->
    <section class="page-title" style="background-image:url( {{ asset('assets/front/images/background/1.jpg')}}) ">
    	<div class="auto-container">
        	<h2>{{ __('message.services') }}</h2>
        </div>
    </section>

    <div class="breadcrumb-outer">
        <div class="auto-container">
            <ul class="bread-crumb text-center">
                <li><a href="{{url('/')}}">{{__('message.home')}}</a> <span class="fa fa-angle-right"></span></li>
                <li>{{ __('message.services') }} </li>
            </ul>
        </div>
    </div>

	<!-- Services Section-->
    <section class="services-section">
    	<div class="auto-container">
    		<!--Title Box-->
    		<div class="title-box">
    			<div class="row clearfix">
                    @foreach ($services as $service)
                        <div class="column col-lg-12 col-md-12 col-sm-12">
                            @if(App::islocale('en'))
                                <h3 style="float:left;margin-right:10px"> {{ __('message.WE_OFFER') }} <br> {{__('message.DIFFERENT_SERVICES') }}</h3>
                                <div class="text"> {{ $service->content }}</div>
                            @elseif(App::islocale('ar'))
                                <h3> {{ __('message.WE_OFFER') }} <br> {{__('message.DIFFERENT_SERVICES') }}</h3>
                                <div> {{ $service->content_ar }}</div>
                            @else
                                <h3 style="float:left;margin-right:10px"> {{ __('message.WE_OFFER') }} <br> {{__('message.DIFFERENT_SERVICES') }}</h3>
                                <div class="text"> {{ $service->content_fr }}</div>
                            @endif
                        </div>
                    @endforeach
				</div>
			</div>
        </div>
    </section>
    <!--End Market Section-->

       <!-- products -->
    <section class="news-section">
        <div class="auto-container">
            <!--Sec Title-->
            <div class="sec-title centered">
                <h2>{{__('message.products')}}</h2>
            </div>
            <div class="row clearfix">
                <!--News Block-->
                @foreach ($products as $product)
                <div class="news-block col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="{{ asset('/'.$product->image) }}" alt="" />
                        </div>
                        @if (App::isLocale('en'))
                        <div class="lower-content">
                            <h5 style="text-align:center"><a>{{ $product->title }}</a></h5>
                        </div>
                        @elseif (App::isLocale('ar'))
                        <div class="lower-content">
                            <h5 style="text-align:center"><a>{{ $product->title_ar }}</a></h5>
                        </div>
                        @else
                        <div class="lower-content">
                            <h5 style="text-align:center"><a>{{ $product->title_fr }}</a></h5>
                        </div>
                        @endif

                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>

    	<!--Team Section-->
	{{-- <section class="team-section">
            <div class="auto-container">
                <!--Sec Title-->
                <div class="sec-title centered">
                    <h2>{{ __('message.advisors')}}</h2>
                </div>

                <div class="row clearfix">
                    @foreach ($advisors as $advisor)
                        <!--Team Member-->
                        <div class="team-member col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box wow fadeIn" data-wow-delay="0ms" data-wow-duration="1500ms">
                                <div class="image-box">
                                    <figure class="image"><img src="{{ asset('/'.$advisor->image) }}" alt=""></figure>
                                </div>
                                <div class="lower-content">
                                    @if (App::isLocale('en'))
                                        <h6><a>{{$advisor->name }}</a></h6>
                                        <div class="designation">{{$advisor->job_title}}</div>
                                    @elseif (App::isLocale('ar'))
                                        <h6><a>{{$advisor->name_ar }}</a></h6>
                                        <div class="designation">{{$advisor->job_title_ar}}</div>
                                    @else
                                        <h6><a>{{$advisor->name }}</a></h6>
                                        <div class="designation">{{$advisor->job_title_fr}}</div>
                                    @endif
                                    <h6>{{$advisor->email }}</h6>
                                    <h6>{{$advisor->phone_number }}</h6>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

            </div>
    </section> --}}
        <!--End Team Section-->

@endsection
