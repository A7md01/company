@extends('front.layouts.app')
@section('content')

<section class="news-section">
    <div class="auto-container">
            <div class="row">
                    <div class="col-md-12">
                        <form method="POST" action="" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label> @lang('lang.name') </label>
                                        <input class="form-control" type="text" name="name">
                                        @if ($errors->has('name'))
                                            <div class="alert alert-danger">{{ $errors->first('name') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                        <label>@lang('lang.image') </label>
                                        <input type="file" class="form-control" name="image">
                                        @if ($errors->has('image'))
                                            <div class="alert alert-danger">{{ $errors->first('image') }}</div>
                                        @endif
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Client ID</label>
                                        <input class="form-control" name="client_id" type="text">
                                        @if ($errors->has('client_id'))
                                            <div class="alert alert-danger">{{ $errors->first('client_id') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>@lang('lang.email')</label>
                                        <input class="form-control" type="email" name="email">
                                        @if ($errors->has('email'))
                                            <div class="alert alert-danger">{{ $errors->first('email') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label> @lang('lang.telephone_no')</label>
                                        <input class="form-control" type="text" name="phone">
                                        @if ($errors->has('phone'))
                                            <div class="alert alert-danger">{{ $errors->first('phone') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>@lang('lang.job_title')</label>
                                        <input class="form-control" name="job_title" type="text">
                                        @if ($errors->has('job_title'))
                                            <div class="alert alert-danger">{{ $errors->first('job_title') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label> @lang('lang.client_company_name') </label>
                                        <input class="form-control" type="text" name="company_name">
                                        @if ($errors->has('company_name'))
                                            <div class="alert alert-danger">{{ $errors->first('company_name') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="submit-section">
                                <button class="btn btn-primary submit-btn">حفظ</button>
                            </div>
                        </form>
                    </div>
                </div>
    </div>
</section>



@endsection
