@extends('front.layouts.app')
@section('content')

	<!--Page Title-->
    <section class="page-title" style="background-image:url( {{ asset('assets/front/images/background/1.jpg')}}) ">
    	<div class="auto-container">
        	<h2>{{ __('message.capital') }}</h2>
        </div>
    </section>

    <!--Breadcrumb-->
    <div class="breadcrumb-outer">
    	<div class="auto-container">
        	<ul class="bread-crumb text-center">
            	<li><a href="{{url('/')}}">{{__('message.home')}}</a> <span class="fa fa-angle-right"></span></li>
                <li>{{ __('message.capital') }} </li>
            </ul>
        </div>
    </div>
    <!--End Page Title-->


    	<!--Testimonial Page Section-->
        <section class="partners-page-section">
            <div class="auto-container">
                   <div class="sec-title">
                    @if(App::islocale('en'))
                    <h3>{{ __('message.head_capital_en') }}</h3>
                    @elseif(App::islocale('ar'))
                    <h3>{{ __('message.head_capital_ar') }}</h3>
                    @else
                    <h3>{{ __('message.head_capital_fr') }}</h3>
                    @endif
                </div>

                <!--Partner Block-->
                <div class="partner-block">
                    <div class="inner-box">
                        @foreach ($capitals as $capital)
                        @if(App::islocale('en'))
                            <h4>{{$capital->en_name }}</h4>
                            <div class="designation">{{$capital->shares }}</div>
                        @elseif(App::islocale('ar'))
                        <h4>{{$capital->name }}</h4>
                        <div class="designation">{{$capital->shares }}</div>
                        @else
                        <h4>{{$capital->fr_name }}</h4>
                        <div class="designation">{{$capital->shares }}</div>
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
        <!--End Market Section-->


@endsection
