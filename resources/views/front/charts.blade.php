@extends('front.layouts.app')
@section('content')

	<!--Page Title-->
    <section class="page-title" style="background-image:url( {{ asset('assets/front/images/background/1.jpg')}}) ">
    	<div class="auto-container">
        	<h2>{{ __('message.organization_chart') }}</h2>
        </div>
    </section>

    <!--Breadcrumb-->
    <div class="breadcrumb-outer">
    	<div class="auto-container">
        	<ul class="bread-crumb text-center">
            	<li><a href="{{url('/')}}">{{__('message.home')}}</a> <span class="fa fa-angle-right"></span></li>
                <li>{{ __('message.organization_chart') }} </li>
            </ul>
        </div>
    </div>
    <!--End Page Title-->
   <!-- charts -->
    <section class="news-section">
        <div class="auto-container">
            <!--Sec Title-->
            <div class="sec-title centered">
                <h2>{{__('message.organization_chart')}}</h2>
            </div>
            <div class="row clearfix">
                <!--News Block-->
                @foreach ($charts as $chart)
                <div class="">
                    <div class="inner-box">
                        @if (App::isLocale('en'))
                        <div>
                            <img src="{{ asset('/'.$chart->en_chart) }}" alt=""  width="1200" height="1200" />
                        </div>

                        @elseif (App::isLocale('ar'))
                        <div class="image">
                            <img style="" src="{{ asset('/'.$chart->ar_chart) }}" alt=""  width="1200" height="1200" />
                        </div>
                        @else
                        <div class="image">
                            <img src="{{ asset('/'.$chart->fr_chart) }}" alt=""  width="1200" height="1200" />
                        </div>
                        @endif
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>

@endsection
