@extends('admin.layouts.app')
@section('content')
    <div class="container">
        <h3 class="jumbotron"> Create New Capitals</h3>
            <form method="POST" action="{{route('capitals.store')}}" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-group">
                    <label>Arabic Title</label>
                    <input type="text" value="" name="name" class="form-control" placeholder="Arabic Title">
                    @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>English Title</label>
                    <input type="text" value="" name="en_name" class="form-control" placeholder="English name">
                    @error('en_name')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>French Title</label>
                    <input type="text" value="" name="fr_name" class="form-control" placeholder="French name">
                    @error('fr_name')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Shares</label>
                    <input type="text" value="" name="shares" class="form-control" placeholder="Number of Shares">
                    @error('shares')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            <button type="submit" class="btn btn-primary" style="margin-top:10px">Upload</button>
        </form>
    </div>


@endsection
