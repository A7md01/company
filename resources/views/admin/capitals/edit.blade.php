@extends('admin.layouts.app')
@section('content')
<section class="content">
    <div class="row">
          <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Quick Example</h3>
                </div>
              <!-- /.box-header -->
              <!-- form start -->
                <form  method="post" action="{{route('capitals.update' , $capitals->id )}}" class="form" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="put">
                    <div class="box-body">
                        <div class="form-group">
                            <label> Arabic name</label>
                            <input type="text" value="{{$capitals->name}}" name="name" class="form-control" >
                            @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>English name</label>
                            <input type="text" value="{{$capitals->en_name}}" name="en_name" class="form-control"   >
                            @error('en_name')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>French name</label>
                            <input type="text" value="{{$capitals->fr_name}}" name="fr_name" class="form-control" >
                            @error('fr_name')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Shares </label>
                            <input type="text" value="{{$capitals->shares}}" name="shares" class="form-control" >
                            @error('shares')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </div>
                </form>
            </div>
            <!-- /.box -->
        </div>
          <!--/.col (right) -->
    </div>
        <!-- /.row -->
</section>

@endsection
