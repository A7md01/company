@extends('admin.layouts.app')
@section('content')
<section class="content">
        <div class="row">
          <!-- left column -->
            <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                <h3 class="box-title">Quick Example</h3>
                </div>
              <!-- /.box-header -->
              <!-- form start -->
            <form  method="post" action="{{route('chooses.update' , $chooses->id )}}" class="form" enctype="multipart/form-data">
                  {{csrf_field()}}
                  <input type="hidden" name="_method" value="put">
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">English Header</label>
                        <input type="text" value="{{$chooses->header}}" name="header" class="form-control"placeholder="English header">
                        @error('header')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Arabic Header</label>
                        <input type="text" value="{{$chooses->header_ar}}" name="header_ar" class="form-control"placeholder="Arabic Header">
                        @error('header_ar')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">French Header</label>
                        <input type="text" value="{{$chooses->header_fr}}" name="header_fr" class="form-control"placeholder="French Header">
                        @error('header_fr')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label >English content</label>
                        <textarea name="content" class="form-control" id="" cols="30" rows="6">{{$chooses->content}}</textarea>
                        @error('content')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label >Arabic content</label>
                        <textarea name="content_ar" class="form-control" id="" cols="30" rows="6">{{$chooses->content_ar}}</textarea>
                        @error('content_ar')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label >French content</label>
                        <textarea name="content_fr" class="form-control" id="" cols="30" rows="6">{{$chooses->content_fr}}</textarea>
                        @error('content_fr')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Edit</button>
                </div>
            </form>
            </div>
            <!-- /.box -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </section>

@endsection
