
@extends('admin.layouts.app')
@section('content')
<section class="content">
        <a href="{{ route('chooses.create') }}" class="btn btn-primary text-capitalize "> add new </a>
            <div class="row">
              <div class="col-md-12">
                <div class="box">
                  <div class="box-header with-border">
                    <h3 class="box-title">Why ASCE</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <table class="table table-bordered">
                      <tr>
                        <th style="width: 10px">#</th>
                        <th>English header</th>
                        <th>Arabic header</th>
                        <th>French header</th>
                        <th>English content </th>
                        <th>Arabic content </th>
                        <th>French content </th>
                        <th style="width: 100px">actions</th>
                      </tr>
                      @foreach ($chooses as $choose)
                            <tr>
                            <td>{{$choose->id}}</td>
                                <td> {{ $choose->header }} </td>
                                <td> {{ $choose->header_ar }} </td>
                                <td> {{ $choose->header_fr }} </td>
                                <td> {{ $choose->content }} </td>
                                <td> {{ $choose->content_ar }} </td>
                                <td> {{ $choose->content_fr }} </td>
                                <td>
                                    <form method="post" action="{{route('chooses.destroy', $choose->id)}}">
                                        <span>
                                            <a href="{{ route('chooses.edit', $choose->id) }}"><i class="fa fa-pencil btn btn-primary"> </i></a>
                                        </span>
                                            {{csrf_field()}}
                                            <input type="hidden" name="_method" value="DELETE" />
                                            <div class="btn-group">
                                                <button  onclick="return confirm(' هل انت متاكد ؟؟')" class="fa fa-trash btn btn-danger" type="submit"></button>
                                            </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                    </table>
                  </div>
                  <!-- /.box-body -->


              </div>

            </div>

          </section>
          <!-- /.content -->

@endsection
