<header class="main-header">
        <!-- Logo -->
    <a href="" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b>LT</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>ASCE Admin</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown messages-menu">
                    <ul class="dropdown-menu">
                </li>
            </ul>
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img src=" {{asset('assets/admin/dist/img/user2-160x160.jpg') }} " class="user-image" alt="User Image">
                    <span class="hidden-xs">admin</span>
                </a>
                <ul class="dropdown-menu">
                    <!-- Menu Footer-->
                    <li>
                        <a href="#" class="btn btn-default btn-flat">Sign out</a>
                    </li>
                </ul>
            </li>
                <!-- Control Sidebar Toggle Button -->
        </div>
    </nav>
</header>
