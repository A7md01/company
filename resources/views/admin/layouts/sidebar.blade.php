<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('assets/admin/dist/img/user2-160x160.jpg') }} " class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>admin</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <ul class="sidebar-menu" data-widget="tree">
            <li>
                <a href="{{url('/admin')}}">
                    <i class="fa fa-files-o"></i>
                    Main dashboard
                </a>
            </li>
            <li>
                <a href="{{ url('admin/sliders')}}">
                    <i class="fa fa-files-o"></i>
                    Slider
                </a>
            </li>
            <li>
                <a href="{{ url('admin/abouts')}}">
                    <i class="fa fa-files-o"></i>
                    About asce
                </a>
            </li>
            <li>
                <a href="{{ url('admin/statistics')}}">
                    <i class="fa fa-files-o"></i>
                    Statistics
                </a>
            </li>
            <li>
                <a href="{{ url('admin/advisors')}}">
                    <i class="fa fa-files-o"></i>
                    Advisors
                </a>
            </li>
            <li>
                <a href="{{ url('admin/chooses')}}">
                    <i class="fa fa-files-o"></i>
                    Why ASCE
                </a>
            </li>
            <li>
                <a href="{{ url('admin/manufacturing')}}">
                    <i class="fa fa-files-o"></i>
                    Manufacturing
                </a>
            </li>
            <li>
                <a href="{{ url('admin/products')}}">
                    <i class="fa fa-files-o"></i>
                    products
                </a>
            </li>
            <li>
                <a href="{{ url('admin/services')}}">
                    <i class="fa fa-files-o"></i>
                    services
                </a>
            </li>
            <li>
                <a href="{{ url('admin/jobs')}}">
                    <i class="fa fa-files-o"></i>
                    Jobs
                </a>
            </li>
            <li>
                <a href="{{ url('admin/charts')}}">
                    <i class="fa fa-files-o"></i>
                    Charts
                </a>
            </li>
            <li>
                <a href="{{ url('admin/capitals')}}">
                    <i class="fa fa-files-o"></i>
                    Capitals
                </a>
            </li>
        </ul>
    </section>
        <!-- /.sidebar -->
</aside>
