
@extends('admin.layouts.app')
@section('content')

<section class="content">
    <a href="{{ route('sliders.create') }}" class="btn btn-primary text-capitalize "> add new </a>
        <div class="row">
          <div class="col-md-12">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Slider</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <table class="table table-bordered">
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Image  </th>
                    <th>Title</th>
                    <th>Title Arabic</th>
                    <th>Title Frensh</th>
                    <th>description</th>
                    <th>description Arabic</th>
                    <th>description Frensh</th>
                    <th style="width: 100px">actions</th>
                  </tr>
                  @foreach ($sliders as $slider)
                        <tr>
                            <td>{{$slider->id}}</td>
                            <td> <img src="{{ asset('/'.$slider->image) }}" height="100px" width="100px" alt="">
                            </td>
                            <td> {{ $slider->title }} </td>
                            <td> {{ $slider->title_ar }} </td>
                            <td> {{ $slider->title_fr }} </td>
                            <td>{{ $slider->description }}</td>
                            <td>{{ $slider->description_ar }}</td>
                            <td>{{ $slider->description_fr }}</td>
                            <td>
                                <form method="post" action="{{route('sliders.destroy', $slider->id)}}">
                                    <span>
                                        <a href="{{ route('sliders.edit', $slider->id) }}"><i class="fa fa-pencil btn btn-primary"> </i></a>
                                    </span>
                                        {{csrf_field()}}
                                        <input type="hidden" name="_method" value="DELETE" />
                                        <div class="btn-group">
                                            <button  onclick="return confirm(' هل انت متاكد ؟؟')" class="fa fa-trash btn btn-danger" type="submit"></button>
                                        </div>
                                </form>
                            </td>
                        </tr>
                    @endforeach

                </table>
              </div>
              <!-- /.box-body -->


          </div>

        </div>

      </section>
      <!-- /.content -->


@endsection
