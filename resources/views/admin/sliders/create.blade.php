@extends('admin.layouts.app')
@section('content')
    <div class="container">
        <h3 class="jumbotron">Slider Section</h3>
            <form method="POST" action="{{route('sliders.store')}}" enctype="multipart/form-data" files="true">
                {{csrf_field()}}
                <div class="input-group control-group increment" >
                    <label for="exampleInputEmail1">Image</label>
                    <input type="file" name="image" class="form-control">
                    @error('image')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="input-group-btn">
                        <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i></button>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">English Title </label>
                    <input type="text" value="" name="title" class="form-control" placeholder="English Title">
                    @error('title')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Arabic Title </label>
                    <input type="text" value="" name="title_ar" class="form-control" placeholder="Arabic Title">
                    @error('title_ar')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Frensh Title </label>
                    <input type="text" value="" name="title_fr" class="form-control" placeholder="Frensh Title ">
                    @error('title_fr')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">English Description </label>
                    <textarea name="description" class="form-control" id="" cols="30" rows="6"></textarea>
                    @error('description')
                        <div class="alert alert-danger">{{$message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Arabic Description </label>
                    <textarea name="description_ar" class="form-control" id="" cols="30" rows="6"></textarea>
                    @error('description_ar')
                        <div class="alert alert-danger">{{$message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">French Description </label>
                    <textarea name="description_fr" class="form-control" id="" cols="30" rows="6"></textarea>
                    @error('description_fr')
                        <div class="alert alert-danger">{{$message }}</div>
                    @enderror
                </div>


            <button type="submit" class="btn btn-primary" style="margin-top:10px">Upload</button>
        </form>
    </div>


@endsection
