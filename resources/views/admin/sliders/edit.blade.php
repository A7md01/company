@extends('admin.layouts.app')
@section('content')
<section class="content">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Quick Example</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form  method="post" action="{{route('sliders.update' , $sliders->id )}}" class="form" enctype="multipart/form-data">
                  {{csrf_field()}}
                  <input type="hidden" name="_method" value="put">
                <div class="box-body">
                    <div class="input-group control-group increment" >
                        <input type="file" name="image" class="form-control">
                        <img src = " {{asset('/' . $sliders->image )}}" height="200px" width="200px" >
                        @error('image')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">English title</label>
                        <input type="text" value="{{$sliders->title}}" name="title" class="form-control" >
                        @error('title')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Arabic title</label>
                        <input type="text" value="{{$sliders->title_ar}}" name="title_ar" class="form-control" >
                        @error('title_ar')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">French title</label>
                        <input type="text" value="{{$sliders->title_fr}}" name="title_fr" class="form-control" >
                        @error('title_fr')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">English description</label>
                        <textarea name="description"  class="form-control" id="" cols="30" rows="10">{{$sliders->description}}</textarea>
                        @error('description')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Arabic description</label>
                        <textarea name="description_ar"  class="form-control" id="" cols="30" rows="10">{{$sliders->description_ar}}</textarea>
                        @error('description_ar')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">French description</label>
                        <textarea name="description_fr"  class="form-control" id="" cols="30" rows="10">{{$sliders->description_fr}}</textarea>
                        @error('description_fr')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Edit</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </section>

@endsection
