@extends('admin.layouts.app')
@section('content')
    <div class="container">
        <h3 class="jumbotron"> Manufacturing </h3>
        <form method="POST" action="{{route('manufacturing.store')}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
                <label>English Header</label>
                <input type="text" value="" name="header" class="form-control" placeholder="English header">
                @error('header')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label>Arabic Header</label>
                <input type="text" value="" name="header_ar" class="form-control" placeholder="Arabic header">
                @error('header_ar')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label>French Header</label>
                <input type="text" value="" name="header_fr" class="form-control" placeholder="French header">
                @error('header_fr')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label >English content</label>
                <textarea name="content" class="form-control" id="" cols="30" rows="6"></textarea>
                @error('content')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label >Arabic content</label>
                <textarea name="content_ar" class="form-control" id="" cols="30" rows="6"></textarea>
                @error('content_ar')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label >French content</label>
                <textarea name="content_fr" class="form-control" id="" cols="30" rows="6"></textarea>
                @error('content_fr')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary" style="margin-top:10px">Upload</button>
        </form>
    </div>


@endsection
