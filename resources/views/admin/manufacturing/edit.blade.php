@extends('admin.layouts.app')
@section('content')
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
        <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                <h3 class="box-title">Quick Example</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form  method="post" action="{{route('manufacturing.update' , $manufacturing->id )}}" class="form" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="put">
                    <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">English Header</label>
                        <input type="text" value="{{$manufacturing->header}}" name="header" class="form-control" >
                        @error('header')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Arabic Header</label>
                        <input type="text" value="{{$manufacturing->header_ar}}" name="header_ar" class="form-control" >
                        @error('header_ar')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">French Header</label>
                        <input type="text" value="{{$manufacturing->header_fr}}" name="header_fr" class="form-control" >
                        @error('header_fr')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">English content</label>
                        <textarea name="content"  class="form-control" id="" cols="30" rows="10">{{$manufacturing->content}}</textarea>
                        @error('content')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Arabic content</label>
                        <textarea name="content_ar"  class="form-control" id="" cols="30" rows="10">{{$manufacturing->content_ar}}</textarea>
                        @error('content_ar')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">French content</label>
                        <textarea name="content_fr"  class="form-control" id="" cols="30" rows="10">{{$manufacturing->content_fr}}</textarea>
                        @error('content_fr')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Edit</button>
                </div>
                </form>
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
    <!-- /.row -->
</section>

@endsection
