
@extends('admin.layouts.app')
@section('content')
<section class="content">
    <a href="{{ route('manufacturing.create') }}" class="btn btn-primary text-capitalize "> add new </a>
        <div class="row">
            <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                <h3 class="box-title"> Manufacturing </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                <table class="table table-bordered">
                    <tr>
                    <th style="width: 10px">#</th>
                    <th>English header</th>
                    <th>Arabic header</th>
                    <th>French header</th>
                    <th>English content </th>
                    <th>Arabic content </th>
                    <th>French content </th>
                    <th style="width: 100px">actions</th>
                    </tr>
                    @foreach ($manufacturing as $manufact)
                        <tr>
                        <td>{{$manufact->id}}</td>
                            <td> {{ $manufact->header }}   </td>
                            <td> {{ $manufact->header_ar }}   </td>
                            <td> {{ $manufact->header_fr }}   </td>
                            <td> {{ $manufact->content }} </td>
                            <td> {{ $manufact->content_ar }} </td>
                            <td> {{ $manufact->content_fr }} </td>
                            <td>
                                <form method="post" action="{{route('manufacturing.destroy', $manufact->id)}}">
                                    <span>
                                        <a href="{{ route('manufacturing.edit', $manufact->id) }}"><i class="fa fa-pencil btn btn-primary"> </i></a>
                                    </span>
                                        {{csrf_field()}}
                                        <input type="hidden" name="_method" value="DELETE" />
                                        <div class="btn-group">
                                            <button  onclick="return confirm(' هل انت متاكد ؟؟')" class="fa fa-trash btn btn-danger" type="submit"></button>
                                        </div>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <!-- /.box-body -->


        </div>

    </div>

</section>
<!-- /.content -->


@endsection
