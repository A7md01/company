
@extends('admin.layouts.app')
@section('content')
<section class="content">
        <a href="{{ route('jobs.create') }}" class="btn btn-primary text-capitalize "> add new </a>
            <div class="row">
              <div class="col-md-12">
                <div class="box">
                  <div class="box-header with-border">
                    <h3 class="box-title">Jobs</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <table class="table table-bordered">
                      <tr>
                        <th style="width: 10px">#</th>
                        <th>Arabic Job Title </th>
                        <th>English Job Title </th>

                        <th>Arabic Job Code </th>
                        <th>English Job Code </th>

                        <th> Arabic Years Of Experience </th>
                        <th> English Years Of Experience </th>

                        <th>Arabic Job Description </th>
                        <th>English Job Description </th>

                        <th>Register End Date </th>
                        <th style="width: 100px">actions</th>
                      </tr>
                      @foreach ($jobs as $job)
                            <tr>
                            <td>{{$job->id}}</td>
                                <td> {{ $job->job_title}} </td>
                                <td> {{ $job->job_title_en}} </td>

                                <td> {{ $job->job_code}} </td>
                                <td> {{ $job->job_code_en}} </td>

                                <td> {{ $job->years_of_experience }} </td>
                                <td> {{ $job->years_of_experience_en }} </td>

                                <td> {{ $job->job_description }} </td>
                                <td> {{ $job->job_description_en }} </td>
                                
                                <td> {{ $job->register_end_date }} </td>
                                <td>
                                    <form method="post" action="{{route('jobs.destroy', $job->id)}}">
                                        <span>
                                            <a href="{{ route('jobs.edit', $job->id) }}"><i class="fa fa-pencil btn btn-primary"> </i></a>
                                        </span>
                                            {{csrf_field()}}
                                            <input type="hidden" name="_method" value="DELETE" />
                                            <div class="btn-group">
                                                <button  onclick="return confirm(' هل انت متاكد ؟؟')" class="fa fa-trash btn btn-danger" type="submit"></button>
                                            </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                    </table>
                  </div>
                  <!-- /.box-body -->


              </div>

            </div>

          </section>
          <!-- /.content -->

@endsection
