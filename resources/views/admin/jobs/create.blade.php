@extends('admin.layouts.app')
@section('content')
    <div class="container">
        <h3 class="jumbotron"> Add new Job</h3>
            <form method="POST" action="{{route('jobs.store')}}" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="exampleInputEmail1"> Arabic Job Title</label>
                    <input type="text" name="job_title" class="form-control"placeholder="Job Title">
                    @error('job_title')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">English  Job Title</label>
                    <input type="text" name="job_title_en" class="form-control"placeholder=" English Job Title">
                    @error('job_title_en')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Arabic Job Code</label>
                    <input type="text" name="job_code" class="form-control"placeholder="Arabic Job Code">
                    @error('job_code')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">English Job Code</label>
                    <input type="text" name="job_code_en" class="form-control"placeholder="English Job Code">
                    @error('job_code_en')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Arabic Years of Exoeriences</label>
                    <input type="text" value="" name="years_of_experience" class="form-control"placeholder="Arabic Years of Exoeriences ">
                    @error('years_of_experience')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1"> English Years of Exoeriences</label>
                    <input type="text" value="" name="years_of_experience_en" class="form-control"placeholder="English Years of Exoeriences ">
                    @error('years_of_experience_en')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label >Arabic Job description</label>
                    <textarea name="job_description" class="form-control" id="" cols="30" rows="6"></textarea>
                    @error('job_description')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label >English Job description</label>
                    <textarea name="job_description_en" class="form-control" id="" cols="30" rows="6"></textarea>
                    @error('job_description_en')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Register End Date</label>
                    <input type="date" value="" name="register_end_date" class="form-control">
                    @error('register_end_date')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            <button type="submit" class="btn btn-primary" style="margin-top:10px">Upload</button>
        </form>
    </div>


@endsection
