@extends('admin.layouts.app')
@section('content')
<section class="content">
        <div class="row">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
                </div>
                @endif

                @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
                @endif
          <!-- left column -->
            <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                <h3 class="box-title">Quick Example</h3>
                </div>
              <!-- /.box-header -->
              <!-- form start -->
            <form  method="post" action="{{route('jobs.update' , $jobs->id )}}" class="form" enctype="multipart/form-data">
                  {{csrf_field()}}
                  <input type="hidden" name="_method" value="put">
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Arabic Job Title</label>
                        <input type="text" value="{{ $jobs->job_title}} " name="job_title" class="form-control">
                        @error('job_title')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">English Job Title</label>
                        <input type="text" value="{{ $jobs->job_title_en}} " name="job_title_en" class="form-control">
                        @error('job_title_en')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Arabic Job Code</label>
                        <input type="text" value="{{ $jobs->job_code }} " name="job_code" class="form-control">
                        @error('job_code')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">English Job Code</label>
                        <input type="text" value="{{ $jobs->job_code_en }} " name="job_code_en" class="form-control">
                        @error('job_code_en')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Arabic Years of Exoeriences</label>
                        <input type="text" value="{{ $jobs->years_of_experience}} " name="years_of_experience" class="form-control">
                        @error('years_of_experience')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">English Years of Exoeriences</label>
                        <input type="text" value="{{ $jobs->years_of_experience_en}} " name="years_of_experience_en" class="form-control">
                        @error('years_of_experience_en')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label >Arabic Job description</label>
                        <textarea name="job_description" class="form-control" id="" cols="30" rows="6"> {{ $jobs->job_description}} </textarea>
                        @error('job_description')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label >English Job description</label>
                        <textarea name="job_description_en  " class="form-control" id="" cols="30" rows="6"> {{ $jobs->job_description_en}} </textarea>
                        @error('job_description_en')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Register End Date</label>
                        <input type="date" value="{{ $jobs->register_end_date }}" name="register_end_date" class="form-control">
                        @error('register_end_date')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Edit</button>
                </div>
            </form>
            </div>
            <!-- /.box -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </section>

@endsection
