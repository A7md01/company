@extends('admin.layouts.app')
@section('content')
<section class="content">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Quick Example</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
            <form  method="post" action="{{route('statistics.update' , $statistics->id )}}" class="form" enctype="multipart/form-data">
                    {{csrf_field()}}
                <input type="hidden" name="_method" value="put">
                <div class="box-body">
                    <div class="form-group">
                        <label>English title</label>
                        <input type="text" value="{{$statistics->title}}" name="title" class="form-control">
                        @error('title')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Arabic title</label>
                        <input type="text" value="{{$statistics->title_ar}}" name="title_ar" class="form-control">
                        @error('title_ar')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>French title</label>
                        <input type="text" value="{{$statistics->title_fr}}" name="title_fr" class="form-control">
                        @error('title_fr')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Number of statistics</label>
                        <input type="number" value="{{$statistics->statistics_no}}" name="statistics_no" class="form-control" >
                        @error('statistics_no')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Edit</button>
                </div>
            </form>
            </div>
            <!-- /.box -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </section>

@endsection
