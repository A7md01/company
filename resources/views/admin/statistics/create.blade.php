@extends('admin.layouts.app')
@section('content')
    <div class="container">
        <h3 class="jumbotron"> Create Statistics </h3>
        <form method="POST" action="{{route('statistics.store')}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
                <label>English title</label>
                <input type="text" value="" name="title" class="form-control" placeholder="English title">
                @error('title')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label>Arabic title</label>
                <input type="text" value="" name="title_ar" class="form-control" placeholder="Arabic title">
                @error('title_ar')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label>French title</label>
                <input type="text" value="" name="title_fr" class="form-control" placeholder="French title">
                @error('title_fr')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label>Number of statistics</label>
                <input type="number" value="" name="statistics_no" class="form-control">
                @error('statistics_no')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary" style="margin-top:10px">Upload</button>
        </form>
    </div>


@endsection
