
@extends('admin.layouts.app')
@section('content')
<section class="content">
        <a href="{{ route('statistics.create') }}" class="btn btn-primary text-capitalize "> add new </a>
            <div class="row">
              <div class="col-md-12">
                <div class="box">
                  <div class="box-header with-border">
                    <h3 class="box-title"> Statistics </h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <table class="table table-bordered">
                      <tr>
                        <th style="width: 10px">#</th>
                        <th>English title</th>
                        <th>Arabic title</th>
                        <th>French title</th>
                        <th>statistics_number </th>
                        <th style="width: 100px">actions</th>
                      </tr>
                      @foreach ($statistics as $statistic)
                            <tr>
                            <td>{{$statistic->id}}</td>
                                <td> {{ $statistic->title }} </td>
                                <td> {{ $statistic->title_ar }} </td>
                                <td> {{ $statistic->title_fr }} </td>
                                <td> {{ $statistic->statistics_no }} </td>
                                <td>
                                    <form method="post" action="{{route('statistics.destroy', $statistic->id)}}">
                                        <span>
                                            <a href="{{ route('statistics.edit', $statistic->id) }}"><i class="fa fa-pencil btn btn-primary"> </i></a>
                                        </span>
                                            {{csrf_field()}}
                                            <input type="hidden" name="_method" value="DELETE" />
                                            <div class="btn-group">
                                                <button  onclick="return confirm(' هل انت متاكد ؟؟')" class="fa fa-trash btn btn-danger" type="submit"></button>
                                            </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                    </table>
                  </div>
                  <!-- /.box-body -->


              </div>

            </div>

          </section>
          <!-- /.content -->

@endsection
