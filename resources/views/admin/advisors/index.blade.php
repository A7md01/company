
@extends('admin.layouts.app')
@section('content')
<section class="content">
        <a href="{{ route('advisors.create') }}" class="btn btn-primary text-capitalize "> add new </a>
            <div class="row">
              <div class="col-md-12">
                <div class="box">
                  <div class="box-header with-border">
                    <h3 class="box-title">Advisors</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <table class="table table-bordered">
                      <tr>
                        <th style="width: 10px">#</th>
                        <th>Image</th>
                        <th>English name</th>
                        <th>Arabic name</th>
                        <th>Job title </th>
                        <th>Job title arabic </th>
                        <th>Job title french </th>
                        <th>Email </th>
                        <th>Phone Number </th>
                        <th style="width: 100px">Actions</th>
                      </tr>
                      @foreach ($advisors as $advisor)
                            <tr>
                            <td>{{$advisor->id}}</td>
                                <td> <img src="{{ asset('/'.$advisor->image) }}" height="100px" width="100px" alt="">
                                <td> {{ $advisor->name }}   </td>
                                <td> {{ $advisor->name_ar }}   </td>
                                <td> {{ $advisor->job_title }} </td>
                                <td> {{ $advisor->job_title_ar }} </td>
                                <td> {{ $advisor->job_title_fr }} </td>
                                <td> {{ $advisor->email }} </td>
                                <td> {{ $advisor->phone_number }} </td>

                                <td>
                                    <form method="post" action="{{route('advisors.destroy', $advisor->id)}}">
                                        <span>
                                            <a href="{{ route('advisors.edit', $advisor->id) }}"><i class="fa fa-pencil btn btn-primary"> </i></a>
                                        </span>
                                            {{csrf_field()}}
                                            <input type="hidden" name="_method" value="DELETE" />
                                            <div class="btn-group">
                                                <button  onclick="return confirm(' هل انت متاكد ؟؟')" class="fa fa-trash btn btn-danger" type="submit"></button>
                                            </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                    </table>
                  </div>
                  <!-- /.box-body -->


              </div>

            </div>

          </section>
          <!-- /.content -->

@endsection
