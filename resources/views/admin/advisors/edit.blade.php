@extends('admin.layouts.app')
@section('content')
<section class="content">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Quick Example</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
                <form  method="post" action="{{route('advisors.update' , $advisors->id )}}" class="form" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="put">
                    <div class="box-body">
                    <div class="input-group control-group increment" >
                        <input type="file" name="image" class="form-control">
                        <img src = " {{asset('/' . $advisors->image )}}" height="200px" width="200px" >
                            @error('image')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">English name</label>
                        <input type="text" value="{{$advisors->name}}" name="name" class="form-control" >
                        @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                            <label for="exampleInputEmail1">Arabic name</label>
                            <input type="text" value="{{$advisors->name_ar}}" name="name_ar" class="form-control" >
                            @error('name_ar')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">English Job Title</label>
                        <input type="text" value="{{$advisors->job_title}}" name="job_title" class="form-control" >
                        @error('job_title')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Arabic Job Title</label>
                        <input type="text" value="{{$advisors->job_title_ar}}" name="job_title_ar" class="form-control" >
                        @error('job_title_ar')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">French Job Title</label>
                        <input type="text" value="{{$advisors->job_title_fr}}" name="job_title_fr" class="form-control" >
                        @error('job_title_Fr')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="email" value="{{$advisors->email}}" name="email" class="form-control" >
                        @error('email')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Phone Number</label>
                        <input type="text" value="{{$advisors->phone_number}}" name="phone_number" class="form-control" >
                        @error('phone_number')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Edit</button>
                </div>
                </form>
            </div>
            <!-- /.box -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </section>

@endsection
