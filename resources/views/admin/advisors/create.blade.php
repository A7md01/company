@extends('admin.layouts.app')
@section('content')
    <div class="container">
        <h3 class="jumbotron"> Advisors</h3>
            <form method="POST" action="{{route('advisors.store')}}" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="input-group control-group increment" >
                    <label for="exampleInputEmail1">Image</label>
                    <input type="file" name="image" class="form-control">
                    @error('image')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="input-group-btn">
                        <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i></button>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">English Name</label>
                    <input type="text" value="" name="name" class="form-control" placeholder="English name">
                    @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Arabic Name</label>
                    <input type="text" value="" name="name_ar" class="form-control" placeholder="Arabic name">
                    @error('name_ar')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">English Job title</label>
                    <input type="text" value="" name="job_title" class="form-control" placeholder="English job title">
                    @error('job_title')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Arabic Job title</label>
                    <input type="text" value="" name="job_title_ar" class="form-control" placeholder="Arabic job title">
                    @error('job_title_ar')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">French Job title</label>
                    <input type="text" value="" name="job_title_fr" class="form-control" placeholder="French job title">
                    @error('job_title_fr')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="email" value="" name="email" class="form-control">
                    @error('email')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Phone number</label>
                    <input type="text" value="" name="phone_number" class="form-control">
                    @error('phone_number')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

            <button type="submit" class="btn btn-primary" style="margin-top:10px">Upload</button>
        </form>
    </div>


@endsection
