@extends('admin.layouts.app')
@section('content')
<section class="content">
    <div class="row">
          <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Quick Example</h3>
                </div>
              <!-- /.box-header -->
              <!-- form start -->
                <form  method="post" action="{{route('products.update' , $products->id )}}" class="form" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="put">
                    <div class="box-body">
                        <div class="input-group control-group increment" >
                            <input type="file" name="image" class="form-control">
                            <img src = " {{asset('/' . $products->image )}}" height="200px" width="200px" >
                            @error('image')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>English Title</label>
                            <input type="text" value="{{$products->title}}" name="title" class="form-control" placeholder="title">
                            @error('title')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Arabic Title</label>
                            <input type="text" value="{{$products->title_ar}}" name="title_ar" class="form-control" placeholder="Arabic Title">
                            @error('title_ar')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>French Title</label>
                            <input type="text" value="{{$products->title_fr}}" name="title_fr" class="form-control" placeholder="French Title">
                            @error('title_fr')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </div>
                </form>
            </div>
            <!-- /.box -->
        </div>
          <!--/.col (right) -->
    </div>
        <!-- /.row -->
</section>

@endsection
