@extends('admin.layouts.app')
@section('content')
    <div class="container">
        <h3 class="jumbotron"> Create New Products</h3>
            <form method="POST" action="{{route('products.store')}}" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="input-group" >
                    <label>Image</label>
                    <input type="file" name="image" class="form-control">
                    @error('image')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                </div>
                <div class="form-group">
                    <label>English Title</label>
                    <input type="text" value="" name="title" class="form-control" placeholder="title">
                    @error('title')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Arabic Title</label>
                    <input type="text" value="" name="title_ar" class="form-control" placeholder="Arabic Title">
                    @error('title_ar')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>French Title</label>
                    <input type="text" value="" name="title_fr" class="form-control" placeholder="French Title">
                    @error('title_fr')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            <button type="submit" class="btn btn-primary" style="margin-top:10px">Upload</button>
        </form>
    </div>


@endsection
