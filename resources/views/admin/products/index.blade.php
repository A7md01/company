
@extends('admin.layouts.app')
@section('content')
<section class="content">
        <a href="{{ route('products.create') }}" class="btn btn-primary text-capitalize "> add new </a>
            <div class="row">
              <div class="col-md-12">
                <div class="box">
                  <div class="box-header with-border">
                    <h3 class="box-title">product </h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <table class="table table-bordered">
                      <tr>
                        <th style="width: 10px">#</th>
                        <th>Image</th>
                        <th>English Title</th>
                        <th>Arabic Title</th>
                        <th>Freach Title</th>
                        <th style="width: 100px">actions</th>
                      </tr>
                      @foreach ($products as $product)
                            <tr>
                            <td>{{$product->id}}</td>
                                <td> <img src="{{ asset('/'.$product->image) }}" height="100px" width="100px" alt="">
                                <td> {{ $product->title }} </td>
                                <td> {{ $product->title_ar }} </td>
                                <td> {{ $product->title_fr }} </td>
                                <td>
                                    <form method="post" action="{{route('products.destroy', $product->id)}}">
                                        <span>
                                            <a href="{{ route('products.edit', $product->id) }}"><i class="fa fa-pencil btn btn-primary"> </i></a>
                                        </span>
                                            {{csrf_field()}}
                                            <input type="hidden" name="_method" value="DELETE" />
                                            <div class="btn-group">
                                                <button  onclick="return confirm(' هل انت متاكد ؟؟')" class="fa fa-trash btn btn-danger" type="submit"></button>
                                            </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                    </table>
                  </div>
                  <!-- /.box-body -->


              </div>

            </div>

          </section>
          <!-- /.content -->

@endsection
