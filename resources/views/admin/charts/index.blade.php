
@extends('admin.layouts.app')
@section('content')

<section class="content">
        <a href="{{ route('charts.create') }}" class="btn btn-primary text-capitalize "> add new </a>
            <div class="row">
              <div class="col-md-12">
                <div class="box">
                  <div class="box-header with-border">
                    <h3 class="box-title">chart </h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <table class="table table-bordered">
                      <tr>
                        <th style="width: 10px">#</th>
                        <th>Arabic Chart</th>
                        <th>English Chart</th>
                        <th>Freach Chart</th>
                        <th style="width: 100px">actions</th>
                      </tr>
                    @foreach ($charts as $chart)
                        <tr>
                            <td>{{$chart->id}}</td>
                            <td> <img src="{{ asset('/'.$chart->ar_chart) }}" height="100px" width="100px" alt="">
                            <td> <img src="{{ asset('/'.$chart->en_chart) }}" height="100px" width="100px" alt="">
                            <td> <img src="{{ asset('/'.$chart->fr_chart) }}" height="100px" width="100px" alt="">
                            <td>
                                <form method="post" action="{{route('charts.destroy', $chart->id)}}">
                                    <span>
                                        <a href="{{ route('charts.edit', $chart->id) }}"><i class="fa fa-pencil btn btn-primary"> </i></a>
                                    </span>
                                        {{csrf_field()}}
                                        <input type="hidden" name="_method" value="DELETE" />
                                        <div class="btn-group">
                                            <button  onclick="return confirm(' هل انت متاكد ؟؟')" class="fa fa-trash btn btn-danger" type="submit"></button>
                                        </div>
                                </form>
                            </td>
                        </tr>
                        @endforeach

                    </table>
                  </div>
                  <!-- /.box-body -->


              </div>

            </div>

          </section>
          <!-- /.content -->

@endsection
