@extends('admin.layouts.app')
@section('content')
<section class="content">
    <div class="row">
          <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Quick Example</h3>
                </div>
              <!-- /.box-header -->
              <!-- form start -->
                <form  method="post" action="{{route('charts.update' , $charts->id )}}" class="form" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="put">
                    <div class="box-body">
                        <div class="input-group control-group increment" >
                            <input type="file" name="ar_chart" class="form-control">
                            <img src = " {{asset('/' . $charts->ar_chart )}}" height="200px" width="200px" >
                            @error('ar_chart')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="input-group control-group increment" >
                            <input type="file" name="en_chart" class="form-control">
                            <img src = " {{asset('/' . $charts->en_chart )}}" height="200px" width="200px" >
                            @error('en_chart')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="input-group control-group increment" >
                            <input type="file" name="fr_chart" class="form-control">
                            <img src = " {{asset('/' . $charts->fr_chart )}}" height="200px" width="200px" >
                            @error('fr_chart')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </div>
                </form>
            </div>
            <!-- /.box -->
        </div>
          <!--/.col (right) -->
    </div>
        <!-- /.row -->
</section>

@endsection
