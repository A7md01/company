@extends('admin.layouts.app')
@section('content')
    <div class="container">
        <h3 class="jumbotron"> Create New charts</h3>
            <form method="POST" action="{{route('charts.store')}}" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="input-group" >
                    <label>Arabic Chart</label>
                    <input type="file" name="ar_chart" class="form-control">
                    @error('ar_chart')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="input-group" >
                    <label>English Chart</label>
                    <input type="file" name="en_chart" class="form-control">
                    @error('en_chart')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="input-group" >
                    <label>French Chart</label>
                    <input type="file" name="fr_chart" class="form-control">
                    @error('fr_chart')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            <button type="submit" class="btn btn-primary" style="margin-top:10px">Upload</button>
        </form>
    </div>
@endsection
