@extends('admin.layouts.app')
@section('content')
    <div class="container">
        <h3 class="jumbotron"> Create services </h3>
        <form method="POST" action="{{route('services.store')}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
                <label>English content</label>
                <input type="text" value="" name="content" class="form-control" placeholder="English content">
                @error('content')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label>Arabic content</label>
                <input type="text" value="" name="content_ar" class="form-control" placeholder="Arabic content">
                @error('content_ar')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label>French content</label>
                <input type="text" value="" name="content_fr" class="form-control" placeholder="French content">
                @error('content_fr')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary" style="margin-top:10px">Upload</button>
        </form>
    </div>


@endsection
