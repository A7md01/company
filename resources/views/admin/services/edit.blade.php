@extends('admin.layouts.app')
@section('content')
<section class="content">
        <div class="row">
          <!-- left column -->
            <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                <h3 class="box-title">Quick Example</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form  method="post" action="{{route('services.update' , $services->id )}}" class="form" enctype="multipart/form-data">
                        {{csrf_field()}}
                    <input type="hidden" name="_method" value="put">
                    <div class="box-body">
                        <div class="form-group">
                            <label>English content</label>
                            <input type="text" value="{{$services->content}}" name="content" class="form-control">
                            @error('content')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Arabic content</label>
                            <input type="text" value="{{$services->content_ar}}" name="content_ar" class="form-control">
                            @error('content_ar')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>French content</label>
                            <input type="text" value="{{$services->content_fr}}" name="content_fr" class="form-control">
                            @error('content_fr')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </div>
                </form>
            </div>
            <!-- /.box -->
        </div>
          <!--/.col (right) -->
    </div>
        <!-- /.row -->
</section>
@endsection
