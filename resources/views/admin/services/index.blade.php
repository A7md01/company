
@extends('admin.layouts.app')
@section('content')
<section class="content">
        <a href="{{ route('services.create') }}" class="btn btn-primary text-capitalize "> add new </a>
            <div class="row">
              <div class="col-md-12">
                <div class="box">
                  <div class="box-header with-border">
                    <h3 class="box-title"> services </h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <table class="table table-bordered">
                      <tr>
                        <th style="width: 10px">#</th>
                        <th>English content</th>
                        <th>Arabic content</th>
                        <th>French content</th>
                        <th style="width: 100px">actions</th>
                      </tr>
                      @foreach ($services as $service)
                            <tr>
                            <td>{{$service->id}}</td>
                                <td> {{ $service->content }} </td>
                                <td> {{ $service->content_ar }} </td>
                                <td> {{ $service->content_fr }} </td>
                                <td>
                                    <form method="post" action="{{route('services.destroy', $service->id)}}">
                                        <span>
                                            <a href="{{ route('services.edit', $service->id) }}"><i class="fa fa-pencil btn btn-primary"> </i></a>
                                        </span>
                                            {{csrf_field()}}
                                            <input type="hidden" name="_method" value="DELETE" />
                                            <div class="btn-group">
                                                <button  onclick="return confirm(' هل انت متاكد ؟؟')" class="fa fa-trash btn btn-danger" type="submit"></button>
                                            </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                    </table>
                  </div>
                  <!-- /.box-body -->


              </div>

            </div>

          </section>
          <!-- /.content -->

@endsection
