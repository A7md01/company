@extends('admin.layouts.app')
@section('content')
<section class="content">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Quick Example</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
            <form  method="post" action="{{route('abouts.update' , $abouts->id )}}" class="form" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="put">
                <div class="box-body">
                    <div class="input-group control-group increment" >
                        <input type="file" name="image" class="form-control">
                        <img src = " {{asset('/' . $abouts->image )}}" height="200px" width="200px" >
                        @error('image')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>English Title</label>
                        <input type="text" value="{{$abouts->title}}" name="title" class="form-control" placeholder="title">
                        @error('title')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Arabic Title</label>
                        <input type="text" value="{{$abouts->title_ar}}" name="title_ar" class="form-control" placeholder="Arabic Title">
                        @error('title_ar')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>French Title</label>
                        <input type="text" value="{{$abouts->title_fr}}" name="title_fr" class="form-control" placeholder="French Title">
                        @error('title_fr')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Number of expertise</label>
                        <input type="number" value="{{$abouts->number_expertise}}" name="number_expertise" class="form-control" id="exampleInputEmail1" >
                        @error('number_expertise')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label >English Content</label>
                        <textarea name="content" class="form-control" id="" cols="30" rows="6">{{$abouts->content}}</textarea>
                        @error('content')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label >Arabic Content</label>
                        <textarea name="content_ar" class="form-control" id="" cols="30" rows="6">{{$abouts->content_ar}}</textarea>
                        @error('content_ar')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label >French Content</label>
                        <textarea name="content_fr" class="form-control" id="" cols="30" rows="6">{{$abouts->content_fr}}</textarea>
                        @error('content_fr')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>English Description </label>
                        <textarea name="description" class="form-control" id="" cols="30" rows="6">{{$abouts->description}}</textarea>
                        @error('description')
                            <div class="alert alert-danger">{{$message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Arabic Description </label>
                        <textarea name="description_ar" class="form-control" id="" cols="30" rows="6">{{$abouts->description_ar}}</textarea>
                        @error('description_ar')
                            <div class="alert alert-danger">{{$message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>French Description </label>
                        <textarea name="description_fr" class="form-control" id="" cols="30" rows="6">{{$abouts->description_fr}}</textarea>
                        @error('description_fr')
                            <div class="alert alert-danger">{{$message }}</div>
                        @enderror
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Edit</button>
                </div>
            </form>
            </div>
            <!-- /.box -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </section>

@endsection
