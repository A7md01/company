@extends('admin.layouts.app')
@section('content')
    <div class="container">
        <h3 class="jumbotron"> About Us</h3>
            <form method="POST" action="{{route('abouts.store')}}" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="input-group" >
                    <label>Image</label>
                    <input type="file" name="image" class="form-control">
                    @error('image')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                </div>
                <div class="form-group">
                    <label>English Title</label>
                    <input type="text" value="" name="title" class="form-control" placeholder="title">
                    @error('title')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Arabic Title</label>
                    <input type="text" value="" name="title_ar" class="form-control" placeholder="Arabic Title">
                    @error('title_ar')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>French Title</label>
                    <input type="text" value="" name="title_fr" class="form-control" placeholder="French Title">
                    @error('title_fr')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Number of expertise</label>
                    <input type="number" value="" name="number_expertise" class="form-control">
                    @error('number_expertise')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label >English Content</label>
                    <textarea name="content" class="form-control" id="" cols="30" rows="6"></textarea>
                    @error('content')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label >Arabic Content</label>
                    <textarea name="content_ar" class="form-control" id="" cols="30" rows="6"></textarea>
                    @error('content_ar')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label >French Content</label>
                    <textarea name="content_fr" class="form-control" id="" cols="30" rows="6"></textarea>
                    @error('content_fr')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>English Description </label>
                    <textarea name="description" class="form-control" id="" cols="30" rows="6"></textarea>
                    @error('description')
                        <div class="alert alert-danger">{{$message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Arabic Description </label>
                    <textarea name="description_ar" class="form-control" id="" cols="30" rows="6"></textarea>
                    @error('description_ar')
                        <div class="alert alert-danger">{{$message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>French Description </label>
                    <textarea name="description_fr" class="form-control" id="" cols="30" rows="6"></textarea>
                    @error('description_fr')
                        <div class="alert alert-danger">{{$message }}</div>
                    @enderror
                </div>


            <button type="submit" class="btn btn-primary" style="margin-top:10px">Upload</button>
        </form>
    </div>


@endsection
