
@extends('admin.layouts.app')
@section('content')
<section class="content">
        <a href="{{ route('abouts.create') }}" class="btn btn-primary text-capitalize "> add new </a>
            <div class="row">
              <div class="col-md-12">
                <div class="box">
                  <div class="box-header with-border">
                    <h3 class="box-title">About US</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <table class="table table-bordered">
                      <tr>
                        <th style="width: 10px">#</th>
                        <th>Image</th>
                        <th>English Title</th>
                        <th>Arabic Title</th>
                        <th>Freach Title</th>
                        <th>English content </th>
                        <th>Arabic content </th>
                        <th>Freach content </th>
                        <th>English description </th>
                        <th>Arabic description </th>
                        <th>Freach description </th>
                        <th>number of expertise </th>
                        <th style="width: 100px">actions</th>
                      </tr>
                      @foreach ($abouts as $about)
                            <tr>
                            <td>{{$about->id}}</td>
                                <td> <img src="{{ asset('/'.$about->image) }}" height="100px" width="100px" alt="">
                                <td> {{ $about->title }}   </td>
                                <td> {{ $about->title_ar }}   </td>
                                <td> {{ $about->title_fr }}   </td>
                                <td> {{ $about->content }} </td>
                                <td> {{ $about->content_ar }} </td>
                                <td> {{ $about->content_fr }} </td>
                                <td> {{ $about->description }} </td>
                                <td> {{ $about->description_ar }} </td>
                                <td> {{ $about->description_fr }} </td>
                                <td> {{ $about->number_expertise }} </td>

                                <td>
                                    <form method="post" action="{{route('abouts.destroy', $about->id)}}">
                                        <span>
                                            <a href="{{ route('abouts.edit', $about->id) }}"><i class="fa fa-pencil btn btn-primary"> </i></a>
                                        </span>
                                            {{csrf_field()}}
                                            <input type="hidden" name="_method" value="DELETE" />
                                            <div class="btn-group">
                                                <button  onclick="return confirm(' هل انت متاكد ؟؟')" class="fa fa-trash btn btn-danger" type="submit"></button>
                                            </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                    </table>
                  </div>
                  <!-- /.box-body -->


              </div>

            </div>

          </section>
          <!-- /.content -->

@endsection
