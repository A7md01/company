<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrganizationChart extends Model
{
    protected $table = 'organization_charts';
    protected $fillable = ['ar_chart','en_chart','fr_chart' ];


    public function setArChartAttribute($image)
    {
        $image = request()->file('ar_chart')->store('images');
        $this->attributes['ar_chart'] = $image;
    }
    public function setEnChartAttribute($image)
    {
        $image = request()->file('en_chart')->store('images');
        $this->attributes['en_chart'] = $image;
    }
    public function setFrChartAttribute($image)
    {
        $image = request()->file('fr_chart')->store('images');
        $this->attributes['fr_chart'] = $image;
    }


}
