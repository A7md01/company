<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Capital extends Model
{
    protected $table = 'company_capitals';
    protected $fillable = [ 'name', 'en_name', 'fr_name', 'shares' ];

}
