<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    public function pricing()
    {
        return $this->belongsTo(Pricing::class);
    }
}
