<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pricing extends Model
{
    protected $fillable = [ 'name', 'price' ];

    public function features(){
        return $this->hasMany(Feature::class);
    }

}
