<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Statistic extends Model
{
    protected $table = 'statistics';
    protected $fillable = ['title' , 'title_ar' , 'title_fr' , 'statistics_no',];

}
