<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = ['title','title_ar','title_fr','image'];

    public function setImageAttribute($image)
    {
        $image = request()->file('image')->store('images');
        $this->attributes['image'] = $image;
    }
}
