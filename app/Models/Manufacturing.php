<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Manufacturing extends Model
{
    protected $table = 'manufacturing';
    protected $fillable = ['header', 'content','header_ar', 'content_ar','header_fr', 'content_fr'];

}
