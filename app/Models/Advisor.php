<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Advisor extends Model
{
    protected $table = 'advisors';
    protected $fillable = ['image' , 'name' ,'name_ar' , 'job_title' , 'job_title_ar' , 'job_title_fr' , 'email' , 'phone_number'];


    public function setImageAttribute($image)
    {
        $image = request()->file('image')->store('images');
        $this->attributes['image'] = $image;
    }

}
