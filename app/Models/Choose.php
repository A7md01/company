<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Choose extends Model
{
    protected $table = 'chooses';
    protected $fillable = ['header', 'content','header_ar', 'content_ar','header_fr', 'content_fr'];


}
