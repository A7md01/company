<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $table = 'jobs';
    protected $fillable = ['job_title', 'years_of_experience' , 'job_description',
                            'job_title_en', 'years_of_experience_en' , 'job_description_en' ,
                            'job_code' , 'job_code_en' ,'register_end_date'];


}
