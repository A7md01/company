<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $table = 'abouts';
    protected $fillable = ['title','title_ar','title_fr', 'content','content_ar','content_fr','image','number_expertise','description','description_ar','description_fr'];

    public function setImageAttribute($image)
    {
        $image = request()->file('image')->store('images');
        $this->attributes['image'] = $image;
    }
}
