<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $table = 'sliders' ;
    protected $fillable = ['title', 'image' , 'description' ,'title_ar', 'title_fr' , 'description_ar' , 'description_fr'];


    public function setImageAttribute($image)
    {
        $image = request()->file('image')->store('images');
        $this->attributes['image'] = $image;
    }
}
