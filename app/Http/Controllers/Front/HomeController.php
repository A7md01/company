<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Service;
use App\Models\Portfolio;
use App\Models\Team;
use App\Models\Testimonial;
use App\Models\Report;
use App\Models\About;
use App\Models\Advisor;
use App\Models\Capital;
use App\Models\Category;
use App\Models\Choose;
use App\Models\Job;
use App\Models\Manufacturing;
use App\Models\OrganizationChart;
use App\Models\Product;
use App\Models\Slider;
use App\Models\Statistic;

class HomeController extends Controller
{


    public function index()
    {
        $sliders  = Slider::all();
        $abouts  = About::all();
        $statistics = Statistic::all();
        $services     = Service::all();
        $advisors = Advisor::all();
        $chooses = Choose::all();
        $products = Product::all();

        return view('front.index', compact('sliders','abouts','products','statistics','advisors','chooses'));
    }
    public function about()
    {
        $advisors  = Advisor::all();
        $abouts = About::all();
        $chooses = Choose::all();
        $products = Product::all();
        return view('front.about', compact('advisors' , 'abouts','chooses','products'));
    }
    public function manufacturing()
    {
        $manufacturing = Manufacturing::all();
        $advisors  = Advisor::all();

        return view('front.manufacturing', compact('advisors' , 'manufacturing'));
    }
    public function contactUs()
    {
        $manufacturing = Manufacturing::all();
        $advisors  = Advisor::all();
        return view('front.contactUs', compact('advisors' , 'manufacturing'));
    }
    public function product()
    {
        $products = Product::all();
        $advisors  = Advisor::all();
        $manufacturing = Manufacturing::all();
        return view('front.product', compact('advisors' , 'products','manufacturing'));
    }
    public function service()
    {
        $services = Service::all();
        $products = Product::all();
        $advisors  = Advisor::all();
        return view('front.service', compact('advisors' , 'services' , 'products'));
    }
    public function job()
    {
        $jobs = Job::all();
        return view('front.job' , compact('jobs') );
    }
    public function charts()
    {
        $charts = OrganizationChart::all();
        return view('front.charts' , compact('charts') );
    }
    public function capital()
    {
        $capitals = Capital::all();
        return view('front.capitals' , compact('capitals') );
    }
    public function register()
    {
        $jobs = Job::all();
        return view('front.register_form' , compact('jobs') );
    }


}
