<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Contact;
use Mail;

class ContactController extends Controller
{


   /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
   public function contactUsPost(Request $request)
   {
        $this->validate($request, [
            'full_name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'message' => 'required'
        ]);
       Contact::create($request->all());
       Mail::send('email', ['name' => 'hello ahmed', 'content' => 'content here'], function ($message)
        {
            $message->from('me@gmail.com', 'Christian Nwamba');
            $message->to('chrisn@scotch.io');
        });
        return back()->with('success', 'Thanks for contacting us!');
   }
}
