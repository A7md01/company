<?php


namespace App\Http\Controllers\Admin;

use App\Models\About;
use App\Models\Advisor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdvisorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $advisors = Advisor ::all();
        return view('admin.advisors.index',compact('advisors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.advisors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image'                     => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name'                      =>'required',
            'name_ar'                   =>'required',
            'job_title'                 => 'required',
            'job_title_ar'              => 'required',
            'job_title_fr'              => 'required',
            'email'                     => 'required',
            'phone_number'              => 'required',
        ]);
            $advisors = Advisor::create($request->all());
            return Redirect('admin/advisors');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $advisors =Advisor::find($id);
        return view('admin.advisors.edit',compact('advisors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'image'                     => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name'                      =>'required',
            'name_ar'                   =>'required',
            'job_title'                 => 'required',
            'job_title_ar'              => 'required',
            'job_title_fr'              => 'required',
            'email'                     => 'required',
            'phone_number'              => 'required',

        ]);
        $advisors = Advisor::find($id);
        $advisors->update($request->all());
        return Redirect('admin/advisors');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $advisors = Advisor::find($id);
        $advisors->delete();
        return Redirect('admin/advisors');
    }
}
