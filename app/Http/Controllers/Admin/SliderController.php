<?php

namespace App\Http\Controllers\Admin;

use App\Models\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slider::all();
        return view('admin.sliders.index',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sliders = Slider::all();
        return view('admin.sliders.create',compact('sliders'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'image'                 => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'title'                 => 'required',
            'title_ar'              => 'required',
            'title_fr'              => 'required',
            'description'           => 'required',
            'description_ar'        => 'required',
            'description_fr'        => 'required',
        ]);

        $sliders = Slider::create($request->all());
        return Redirect('admin/sliders');

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $sliders =Slider::find($id);
        return view('admin.sliders.edit',compact('sliders' ));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'image'                 => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'title'                 => 'required',
            'title_ar'              => 'required',
            'title_fr'              => 'required',
            'description'           => 'required',
            'description_ar'        => 'required',
            'description_fr'        => 'required',
        ]);

        $sliders = Slider::find($id);
        $sliders->update($request->all());
        return Redirect('admin/sliders');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sliders = Slider::find($id);
        $sliders->delete();
        return Redirect('admin/sliders');
    }
}
