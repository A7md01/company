<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Testimonial;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testimonials = Testimonial::all();
        return view('admin.testimonials.index',compact('testimonials'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.testimonials.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'img'   => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name'  =>'required',
            'content' => 'required'
        ]);
        if($request->hasfile('img'))
        {
            $imageName = time().'.'.request()->img->getClientOriginalExtension();
            request()->img->move(public_path('images'), $imageName);
            $testimonials = new Testimonial([
                'img'       =>  $imageName,
                'name'      => $request->name,
                'content'     =>  $request->content
            ]);
            $testimonials->save();
            // return 'fdf';
            return Redirect('admin/testimonials');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $testimonials =Testimonial::find($id);
        return view('admin.testimonials.edit',compact('testimonials'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $imageName = time().'.'.request()->img->getClientOriginalExtension();
        $request()->img->move(public_path('images'), $imageName);
        $testimonials = Testimonial::find($id);
        $testimonials->update([
            $testimonials->img = $imageName,
            $testimonials->name = $request->name,
            $testimonials->content = $request->content

        ]);
        $testimonials->save();
        return Redirect('admin/testimonials');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $testimonials = Testimonial::find($id);
        $testimonials->delete();
        return Redirect('admin/testimonials');
    }
}
