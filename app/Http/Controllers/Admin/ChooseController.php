<?php


namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Choose;

class ChooseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $chooses = Choose::all();
        return view('admin.chooses.index',compact('chooses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.chooses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'header'                        =>'required',
            'content'                       => 'required',
            'header_ar'                     =>'required',
            'content_ar'                    => 'required',
            'header_fr'                     =>'required',
            'content_fr'                    => 'required',

        ]);
            $chooses = Choose::create($request->all());
            return Redirect('admin/chooses');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $chooses =Choose::find($id);
        return view('admin.chooses.edit',compact('chooses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'header'                        =>'required',
            'content'                       => 'required',
            'header_ar'                     =>'required',
            'content_ar'                    => 'required',
            'header_fr'                     =>'required',
            'content_fr'                    => 'required',
        ]);
        $chooses = Choose::find($id);
        $chooses->update($request->all());
        return Redirect('admin/chooses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $chooses = Choose::find($id);
        $chooses->delete();
        return Redirect('admin/chooses');
    }
}
