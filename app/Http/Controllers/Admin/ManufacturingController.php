<?php


namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Manufacturing;

class ManufacturingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $manufacturing = Manufacturing::all();
        return view('admin.manufacturing.index',compact('manufacturing'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.manufacturing.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'header'                       =>'required',
            'content'                      => 'required',
            'header_ar'                    =>'required',
            'content_ar'                   => 'required',
            'header_fr'                    =>'required',
            'content_fr'                   => 'required',

        ]);
            $manufacturing = Manufacturing::create($request->all());
            return Redirect('admin/manufacturing');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $manufacturing =Manufacturing::find($id);
        return view('admin.manufacturing.edit',compact('manufacturing'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'header'                       =>'required',
            'content'                      => 'required',
            'header_ar'                    =>'required',
            'content_ar'                   => 'required',
            'header_fr'                    =>'required',
            'content_fr'                   => 'required',

        ]);
        $manufacturing = Manufacturing::find($id);
        $manufacturing->update($request->all());
        return Redirect('admin/manufacturing');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $manufacturing = Manufacturing::find($id);
        $manufacturing->delete();
        return Redirect('admin/manufacturing');
    }
}
