<?php


namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\OrganizationChart;

class OrganizationChartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $charts = OrganizationChart::all();
        return view('admin.charts.index',compact('charts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.charts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'ar_chart'                         => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'en_chart'                         => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'fr_chart'                         => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);
            $charts = OrganizationChart::create($request->all());
            return Redirect('admin/charts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $charts =OrganizationChart::find($id);
        return view('admin.charts.edit',compact('charts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'image'                         => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'title'                         =>'required',
            'title_ar'                      =>'required',
            'title_fr'                      =>'required',
        ]);
        $charts = OrganizationChart::find($id);
        $charts->update($request->all());
        return Redirect('admin/charts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $charts = OrganizationChart::find($id);
        $charts->delete();
        return Redirect('admin/charts');
    }
}
