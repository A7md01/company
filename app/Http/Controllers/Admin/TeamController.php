<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Team;
use Illuminate\Support\Facades\Redirect;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = Team::all();
        return view('admin.teams.index',compact('teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.teams.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'img'   => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name'  =>'required',
            'job_title' => 'required'
        ]);

            $imageName = time().'.'.request()->img->getClientOriginalExtension();
            request()->img->move(public_path('images'), $imageName);
            $teams = new Team([
                'img'       =>  $imageName,
                'name'      => $request->name,
                'job_title'     =>  $request->job_title
            ]);
            $teams->save();
            // return 'fdf';
            return Redirect('admin/teams');

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teams =Team::find($id);
        return view('admin.teams.edit',compact('teams'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $imageName = time().'.'.request()->img->getClientOriginalExtension();
        request()->img->move(public_path('images'), $imageName);
        $teams = Team::find($id);
        $teams->update([
            $teams->img = $imageName,
            $teams->name = $request->name,
            $teams->job_title = $request->job_title

        ]);
        $teams->save();
        return Redirect('admin/teams');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $teams = Team::find($id);
        $teams->delete();
        return Redirect('admin/teams');
    }
}
