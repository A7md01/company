<?php


namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\About;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $abouts = About::all();
        return view('admin.abouts.index',compact('abouts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.abouts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image'                         => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'title'                         =>'required',
            'title_ar'                      =>'required',
            'title_fr'                      =>'required',
            'content'                       => 'required',
            'content_ar'                    => 'required',
            'content_fr'                    => 'required',
            'number_expertise'              => 'required',
            'description'                   => 'required',
            'description_ar'                => 'required',
            'description_fr'                => 'required',
        ]);
            $abouts = About::create($request->all());
            return Redirect('admin/abouts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $abouts =About::find($id);
        return view('admin.abouts.edit',compact('abouts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'image'                         => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'title'                         =>'required',
            'title_ar'                      =>'required',
            'title_fr'                      =>'required',
            'content'                       => 'required',
            'content_ar'                    => 'required',
            'content_fr'                    => 'required',
            'number_expertise'              => 'required',
            'description'                   => 'required',
            'description_ar'                => 'required',
            'description_fr'                => 'required',
        ]);
        $abouts = About::find($id);
        $abouts->update($request->all());
        return Redirect('admin/abouts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $abouts = About::find($id);
        $abouts->delete();
        return Redirect('admin/abouts');
    }
}
