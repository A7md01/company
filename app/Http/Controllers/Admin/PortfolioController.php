<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Portfolio;
use App\Models\Category;
use App\Models\Feature;

class PortfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $portfolios = Portfolio::all();
        return view('admin.portfolios.index', compact('portfolios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.portfolios.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        return $request->features;
        $this->validate($request, [

            'img'           => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'title'         => 'required',
            'category_id'   => 'required',
        ]);

        $imageName = time().'.'.request()->img->getClientOriginalExtension();
        request()->img->move(public_path('images'), $imageName);
        $portfolios = new Portfolio([
            'img'             =>  $imageName,
            'title'           =>  $request->title,
            'category_id'     =>  $request->category_id,
        ]);
        $portfolios->save();

        // foreach ($request->features as $feature) {
        //     Feature::create(['title' => $feature, 'pricing_id' => $portfolios->id]);
        // }

        // return 'fdf';
        return Redirect('admin/portfolios');

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $portfolios =Portfolio::find($id);
        $categories = Category::all();
        return view('admin.portfolios.edit',compact('portfolios', 'categories'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $imageName = time().'.'.request()->img->getClientOriginalExtension();
        request()->img->move(public_path('images'), $imageName);
        $portfolios = Portfolio::find($id);
        $portfolios->update([
            $portfolios->img = $imageName,
            $portfolios->title = $request->title,
            $portfolios->category_id = $request->category_id,

        ]);
        $portfolios->save();
        return Redirect('admin/portfolios');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $portfolios = Portfolio::find($id);
        $portfolios->delete();
        return Redirect('admin/portfolios');
    }
}
