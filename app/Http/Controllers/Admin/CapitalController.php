<?php


namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Capital;

class CapitalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $capitals = Capital::all();
        return view('admin.capitals.index',compact('capitals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.capitals.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'                         =>'required',
            'en_name'                      =>'required',
            'fr_name'                      =>'required',
            'shares'                       =>'required',
        ]);
            $capitals = Capital::create($request->all());
            return Redirect('admin/capitals');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $capitals =Capital::find($id);
        return view('admin.capitals.edit',compact('capitals'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'                         =>'required',
            'en_name'                      =>'required',
            'fr_name'                      =>'required',
            'shares'                       =>'required',
        ]);
        $capitals = Capital::find($id);
        $capitals->update($request->all());
        return Redirect('admin/capitals');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $capitals = Capital::find($id);
        $capitals->delete();
        return Redirect('admin/capitals');
    }
}
