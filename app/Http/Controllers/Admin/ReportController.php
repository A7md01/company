<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Report;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reports = Report::all();
        return view('admin.reports.index',compact('reports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.reports.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'img'   => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'title'  =>'required',
            'content' => 'required'
        ]);
        if($request->hasfile('img'))
        {
            $imageName = time().'.'.request()->img->getClientOriginalExtension();
            request()->img->move(public_path('images'), $imageName);
            $reports = new Report([
                'img'       =>  $imageName,
                'title'      => $request->title,
                'content'     =>  $request->content
            ]);
            $reports->save();
            // return 'fdf';
            return Redirect('admin/reports');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reports =Report::find($id);
        return view('admin.reports.edit',compact('reports'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $imageName = time().'.'.request()->img->getClientOriginalExtension();
        $request()->img->move(public_path('images'), $imageName);
        $reports = Report::find($id);
        $reports->update([
            $reports->img = $imageName,
            $reports->title = $request->title,
            $reports->content = $request->content

        ]);
        $reports->save();
        return Redirect('admin/reports');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reports = Report::find($id);
        $reports->delete();
        return Redirect('admin/reports');
    }
}
