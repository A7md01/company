<?php


namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Job;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = Job::all();
        return view('admin.jobs.index',compact('jobs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.jobs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'job_title'                                  =>'required',
            'job_code'                                   =>'required',
            'job_code_en'                                =>'required',
            'register_end_date'                          =>'required',
            'years_of_experience'                        =>'required',
            'job_description'                            =>'required',
            'years_of_experience_en'                     =>'required',
            'job_description_en'                         =>'required',
            'job_title_en'                               =>'required',

        ]);
            $jobs = Job::create($request->all());
            return Redirect('admin/jobs');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jobs =Job::find($id);
        return view('admin.jobs.edit',compact('jobs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'job_title'                                  =>'required',
            'job_code'                                   =>'required',
            'job_code_en'                                =>'required',
            'register_end_date'                          =>'required',
            'years_of_experience'                        =>'required',
            'job_description'                            =>'required',
            'years_of_experience_en'                     =>'required',
            'job_description_en'                         =>'required',
            'job_title_en'                               =>'required',

        ]);
        $jobs = Job::find($id);
        $jobs->update($request->all());
        return Redirect('admin/jobs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jobs = Job::find($id);
        $jobs->delete();
        return Redirect('admin/jobs');
    }
}
